#coding=utf-8
import os

# key = 'ltr'
# key = 'all'
# key = 'random'
key = 'all_no_bi'

data_dir = '../Preprocess/data/processed/english/arguments/' + key

train_data_path =  data_dir + '/train.data.en.txt'
test_data_path = data_dir + '/test.data.en.txt'
dev_data_path = data_dir + '/dev.data.en.txt'

char_vocab_path = data_dir + '/char_vocab.txt'
word_vocab_path = data_dir + '/word_vocab.txt'
event_type_vocab_path = data_dir + '/event_type_vocab.txt'
event_sub_type_vocab_path = data_dir + '/event_sub_type_vocab.txt'
argument_role_vocab_path = data_dir + '/argument_role_vocab.txt'
argument_role_vocab_bio_path = data_dir + '/argument_role_vocab_bio.txt'
entity_type_vocab_path = data_dir + '/entity_type_vocab.txt'
entity_sub_type_vocab_path = data_dir + '/entity_sub_type_vocab.txt'
pos_tag_vocab_path = data_dir + '/pos_tag_vocab.txt'

dimension = 300
seg_dimension = 4
seg_max = 20
char_word2vec_path = data_dir + '/glove.6B.' + str(dimension) +  'd.txt'

# position embedding

trimmed_char_word2vec_path = data_dir + '/trimmed_char_word2vec.npz'
trimmed_word_word2vec_path = data_dir + '/trimmed_word_word2vec.npz'

train_data_id_path = data_dir + '/train.id.txt'
dev_data_id_path = data_dir + '/dev.id.txt'
test_data_id_path = data_dir + '/test.id.txt'

train_data_seg_path = data_dir + '/train.id.segmented.txt'
dev_data_seq_path = data_dir + '/dev.id.segmented.txt'
test_data_seg_path = data_dir + '/test.id.segmented.txt'

train_data_id_lm_path = data_dir + '/train.id.lm.txt'
dev_data_id_lm_path = data_dir + '/dev.id.lm.txt'
test_data_id_lm_path = data_dir + '/test.id.lm.txt'

seg_mode = 'BMES'
layer_mode = 'layer.6'
full_train_data_id_lm_path = data_dir + '/'+ seg_mode + '/' + layer_mode + '/train.dev.id.lm.full.txt'
full_test_data_id_lm_path = data_dir + '/'+ seg_mode + '/' + layer_mode + '/test.id.lm.full.txt'
full_dev_data_id_lm_path = data_dir + '/'+ seg_mode+ '/' + layer_mode + '/dev.id.lm.full.txt'

mtl_train_data_path = data_dir + '/'+ seg_mode + '/' + layer_mode + '/train.dev.mtl.txt'
mtl_test_data_path = data_dir + '/'+ seg_mode + '/' + layer_mode + '/test.mtl.txt'
mtl_dev_data_path = data_dir + '/'+ seg_mode + '/' + layer_mode + '/dev.mtl.txt'


# BERT相关的设置
bert_dir = os.path.abspath('.') + '/model/bert_as_feature/'
bert_train_input = bert_dir + 'train.input.txt'
bert_train_output = bert_dir + 'train.output.jsonl'

bert_dev_input = bert_dir + 'dev.input.txt'
bert_dev_output = bert_dir + 'dev.output.jsonl'

bert_test_input = bert_dir + 'test.input.txt'
bert_test_output = bert_dir + 'test.output.jsonl'

bert_output = bert_dir + 'output.jsonl'

# shared global variables to be imported from model also
UNK_TOKEN = "#UNK#"
# NUM_TOKEN = "$NUM$"
NONE_TOKEN = "O"

# LM embedding相关
model_dir = os.path.abspath('.') + '/model/bert_as_feature/chinese_L-12_H-768_A-12/'
bert_vocab_file = model_dir + 'vocab.txt'
bert_config_file = model_dir + 'bert_config.json'
bert_init_checkpoint = model_dir + 'bert_model.ckpt'
bert_layers = [-1]
lm_embedding_dim = 768

# 表示合并方法
concat_method = 'concat'
sum_method = 'sum'
gated_method = 'gated'

# joint loss logits计算方法
max_mode = 'max'
sum_mode = 'sum'
average_mode = 'average'