# 1. update the entity selector
for each sentence{
    loop{
        1. get representation for current environment
        2. forward propagation of entity selector and pick an entity e_k as argument candidate
        3. forward propagation of argument detector of entity e_k and predict the argument role for e_k
        4. update the environment (predicted argument roles)
    }until all entities are picked
    calculate reward for entity prediction in this sentence
    reward = -logp(y_i)
    reward = - math.log(1 - math.e ** (-reward))

    back propagation of entity selector
}

# 2. update the argument detector
for each sentence{
    loop{
        1. get representation for current environment
        2. forward propagation of entity selector and pick an entity e_k as argument candidate
        3. forward propagation of argument detector of entity e_k and predict the argument role for e_k
        4. back propagation of entity selector with e_k as training data
        5. update the environment (predicted argument roles)
    }until all entities are picked
}