#coding=utf-8
import os
import tensorflow as tf

import sys
sys.path.append('..')
from EnglishArgumentDetectionModels.configure import *
from .hyperparameter import *

class base_model(object):
	"""docstring for BaseModel"""
	def __init__(self, hp, logger, fine_tune = False):
		self.sess = None # sess
		self.saver = None # 存储模型
		self.logger = logger # 日志
		self.hp = hp # 超参数
		self.fine_tune = fine_tune
		self.best_score = 0


	def reinitialize_weights(self, scope_name):
		'''
		对某一scope的变量进行重新初始化
		'''
		# 获取该scope的所有变量
		variables = tf.contrib.framework.get_variables(scope_name)
		# 对变量进行初始化
		init = tf.variables_initializer(variables)
		self.sess.run(init)


	def add_train_op(self, lr_method, lr, loss, clip = -1):
		'''
		定义self.train_op来进行训练操作
		Args:
			lr_method: adam/adagrad/sgd/rmsprop
			lr: learning rate
			loss: 损失函数
			clip: 梯度的clipping值，如果<0，那么就不clipping
		'''
		_lr_m = lr_method.lower()
		with tf.variable_scope("train_step"):
			if _lr_m == 'adam':
				optimizer = tf.train.AdamOptimizer(lr)
			elif _lr_m == 'adagrad':
				optimizer = tf.train.AdagradOptimizer(lr)
			elif _lr_m == 'sgd':
				optimizer = tf.train.GradientDescentOptimizer(lr)
			elif _lr_m == 'rmsprop':
				optimizer = tf.train.RMSPropOptimizer(lr)
			else:
				raise NotImplementedError("Unknown method {}".format(_lr_m))

		# 获取训练参数
		variables = self.get_variables_for_training()
		self.logger.info(variables)
		self.logger.info('参数数量为:'+str(len(variables)))

		# 进行clip
		if clip > 0:
			self.grads, self.vs = zip(*optimizer.compute_gradients(loss, var_list = variables))
			grads, gnorm  = tf.clip_by_global_norm(self.grads, clip)
			self.train_op = optimizer.apply_gradients(zip(grads, self.vs))
		else:
			self.train_op = optimizer.minimize(loss, var_list = variables)
		self.optimizer = optimizer

	def initialize_session(self):
		'''
		定义self.sess并且初始化变量和self.saver
		'''
		self.logger.info("Initializing tf session")
		self.sess = tf.Session()
		self.sess.run(tf.global_variables_initializer())
		self.saver = tf.train.Saver(tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES))

	def restore_session(self, dir_model = None):
		'''
		reload weights into self.session
		Args:
			dir_model: 模型路径
		'''
		if dir_model is None:
			# 从config中读取模型路径
			dir_model = self.hp.dir_model

		self.logger.info("Reloading the latest trained model...")
		self.saver.restore(self.sess, dir_model)

	def save_session(self):
		'''
		存储session
		'''
		if not os.path.exists(self.hp.dir_model):
			os.makedirs(self.hp.dir_model)
		self.saver.save(self.sess, self.hp.dir_model)

	def close_session(self):
		'''
		close Session
		'''
		self.sess.close()

	def add_summary(self):
		'''
		为tensorboard定义变量, 输出文件为dir_output

		'''
		self.merged = tf.summary.merge_all()
		self.file_writer = tf.summary.FileWriter(self.hp.dir_output,
			self.sess.graph)

	def train(self, train, dev, test):
		'''
		进行训练，采用了early stopping和学习率指数递减
		Args:
			train: dataset yields tuple of (sentence, tags)
			dev: dataset
		'''

		num_of_epoch_no_imprv = 0 # for early stopping，用来记录几个epoch没有提高了
		self.add_summary() # tensorboard
		for epoch in range(self.hp.epochs):
			self.logger.info('Epoch {:} out of {:}'.format(epoch + 1, self.hp.epochs))
			
			# 运行一个epoch的训练工作，并返回在dev数据集上的测试f1
			score = self.run_epoch(train, dev, test, epoch)
			# 进行learning rate decay
			self.hp.learning_rate *= self.hp.learning_rate_decay

			# 进行early stopping并且保存最好的参数
			# 如果效果更好了
			if score > self.best_score:
				# 清零
				num_of_epoch_no_imprv = 0
				# 记录当前的参数
				self.save_session()
				# 更新best score
				self.best_score = score
				self.logger.info("- new best score! ")
			# 如果效果没有更好
			else:
				num_of_epoch_no_imprv += 1
				# 如果已经好多轮没有效果更好了，并且超过了25轮
				if num_of_epoch_no_imprv >= self.hp.num_of_epoch_no_imprv and epoch > self.hp.least_epochs:
					self.logger.info("- early stopping {} epochs without "\
						"improvement".format(self.hp.num_of_epoch_no_imprv))
					break
			# break
		self.logger.info('Training model is over')


	def evaluate(self, test, output_all_file = None, output_wrong_file = None, output_class_wrong_file = None):
		'''
		在测试集上对模型进行测试
		Args:
			test: datset from test.txt
		'''
		self.logger.info("Testing model over test set")
		# 跑测试
		metrics = self.run_evaluate(test, output_all_file, output_wrong_file, output_class_wrong_file)
		msg = " - ".join(["{} {:04.2f}".format(k, v) for k, v in metrics.items()])
		self.logger.info(msg)







