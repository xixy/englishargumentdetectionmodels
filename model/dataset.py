#coding=utf-8
import random
import sys
import os
sys.path.append(os.path.abspath('..'))
# from ArgumentDetectionModels.configure import *
def strQ2B(ustring):
	"""全角转半角"""
	rstring = ""
	for uchar in ustring:
		inside_code=ord(uchar)
		if inside_code == 12288:
		#全角空格直接转换
			inside_code = 32 
		elif (inside_code >= 65281 and inside_code <= 65374):
			#全角字符（除空格）根据关系转化
			inside_code -= 65248

		rstring += chr(inside_code)
	# if rstring != ustring:
	# 	print('corrected')
	return rstring




class dataset(object):
	"""用来处理文本数据，并且提供数据"""
	def __init__(self, filename, shuffle = True, max_iter = None, idlized = False):
		'''
		Args:
			filename: path to the data file
			max_iter: max number of sentences to yield from this dataset
		'''
		self.shuffle = shuffle
		self.filename = filename
		self.max_iter = max_iter
		self.length = None
		self.idlized = idlized
		self.data = []
		for(event_type, argument_role, chars, entity_sub_types, entity_types, \
			roles, distances_1, distances_2, event_sub_types, pos_tag_types, lexical_trigger_labels, lexical_entity_labels) in self:
			self.data.append((event_type, argument_role, chars, \
				entity_sub_types, entity_types, roles, distances_1, \
				distances_2, event_sub_types, pos_tag_types, lexical_trigger_labels, lexical_entity_labels))

		self.sentence_data = None
		self.initiate_sentence_data()

	def __iter__(self):
		'''
		支持iteration的遍历操作，每次得到一个句子的词和标注
		Return:
			words[w1, w2, ..., wn]
			tags:[t1, t2, ..., tn]
		'''
		count = 0
		with open(self.filename) as f:
			chars, entity_sub_types, entity_types, roles, distances_1, distances_2, event_sub_types, pos_tag_types = \
			[], [], [], [], [], [], [], []
			lexical_trigger_labels, lexical_entity_labels = [], []

			event_type, argument_role = None, None
			datas = []
			for line in f:
				# print line
				line = line.strip()
				# 如果是空行，表示句子开始
				if len(line) == 0:
					count += 1
					if self.max_iter is not None and count > self.max_iter:
						break
					yield event_type, argument_role, chars, entity_sub_types, entity_types, \
						roles, distances_1, distances_2, event_sub_types, pos_tag_types, \
						lexical_trigger_labels, lexical_entity_labels
					chars, entity_sub_types, entity_types, roles, distances_1, \
						distances_2, event_sub_types, pos_tag_types = [], [], [], [], [], [], [], []
					lexical_trigger_labels, lexical_entity_labels = [], []
					event_type, argument_role = None, None
				else:
					elems = line.split(' ')
					# 如果需要进行数字化
					if self.idlized:
						elems = [int(x) for x in elems]
					# 如果是信息行
					if len(elems) == 2:
						event_type, argument_role = elems[:2]
					# 如果是token标注行
					elif len(elems) == 8:
						chars.append(elems[0])

						entity_sub_types.append(elems[1])
						entity_types.append(elems[2])
						roles.append(elems[3])
						distances_1.append(elems[4])
						# 如果是trigger
						if elems[4] == 0:
							lexical_trigger_labels.append(elems[0])
						distances_2.append(elems[5])
						# 如果是argument
						if elems[5] == 0:
							lexical_entity_labels.append(elems[0])
						event_sub_types.append(elems[6])
						pos_tag_types.append(elems[7])



	def __len__(self):
		'''
		查看句子数量
		'''
		if self.length is None:
			self.length = 0
			for _ in self:
				self.length += 1
		return self.length

	def initiate_sentence_data(self):
		self.sentence_data = []
		# 初始化
		former_chars_batch = None
		event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
			entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, \
			event_sub_types_batch, pos_tag_types_batch, lexical_trigger_labels_batch, lexical_entity_labels_batch \
			 = [], [], [], [], [], [], [], [], [], [], [], []
		# 遍历标注结果对句子进行处理
		for(event_type, argument_role, chars, entity_sub_types, entity_types, roles, distances_1, distances_2, event_sub_types, pos_tag_types, lexical_trigger_labels, lexical_entity_labels) in self.data:
			# 一个句子遍历结束
			if former_chars_batch != None and former_chars_batch != chars:
				self.sentence_data.append([event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
					entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, event_sub_types_batch, pos_tag_types_batch, lexical_trigger_labels_batch, lexical_entity_labels_batch])
				event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
					entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, \
					event_sub_types_batch, pos_tag_types_batch, lexical_trigger_labels_batch, lexical_entity_labels_batch \
					 = [], [], [], [], [], [], [], [], [], [], [], []

			# 如果相同或者刚开始
			former_chars_batch = chars
			event_type_batch.append(event_type)
			argument_role_batch.append(argument_role)
			chars_batch.append(chars)
			entity_sub_types_batch.append(entity_sub_types)
			entity_types_batch.append(entity_types)
			roles_batch.append(roles)
			distances_1_batch.append(distances_1)
			distances_2_batch.append(distances_2)
			event_sub_types_batch.append(event_sub_types)
			pos_tag_types_batch.append(pos_tag_types)
			lexical_trigger_labels_batch.append(lexical_trigger_labels)
			lexical_entity_labels_batch.append(lexical_entity_labels)


		# 处理最后不足一个batch的情况
		if len(event_type_batch) > 0:
				self.sentence_data.append([event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
					entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, event_sub_types_batch, \
					pos_tag_types_batch, lexical_trigger_labels_batch, lexical_entity_labels_batch])		


	def get_sentence_batch(self):
		if self.shuffle:
			random.shuffle(self.sentence_data)
		for sentence in self.sentence_data:
			yield sentence


	# def get_sentence_batch(self):
	# 	'''
	# 	从数据集中获取minibatch，但是以句子为单位，交给模型去进行遍历，这个时候需要注意的是肯定不做shuffle，数量是句子里的entity数量
	# 	'''
	# 	former_chars_batch = None
	# 	event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
	# 		entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, event_sub_types_batch, lexical_trigger_labels_batch, lexical_entity_labels_batch \
	# 		 = [], [], [], [], [], [], [], [], [], [], []
	# 	for(event_type, argument_role, chars, entity_sub_types, entity_types, roles, distances_1, distances_2, event_sub_types, lexical_trigger_labels, lexical_entity_labels) in self.data:
	# 		if former_chars_batch != None and former_chars_batch != chars:
	# 			yield event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
	# 				entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, event_sub_types_batch, lexical_trigger_labels_batch, lexical_entity_labels_batch
	# 			event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
	# 				entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, event_sub_types_batch, lexical_trigger_labels_batch, lexical_entity_labels_batch \
	# 				 = [], [], [], [], [], [], [], [], [], [], []

	# 		# 如果相同或者刚开始
	# 		former_chars_batch = chars
	# 		event_type_batch.append(event_type)
	# 		argument_role_batch.append(argument_role)
	# 		chars_batch.append(chars)
	# 		entity_sub_types_batch.append(entity_sub_types)
	# 		entity_types_batch.append(entity_types)
	# 		roles_batch.append(roles)
	# 		distances_1_batch.append(distances_1)
	# 		distances_2_batch.append(distances_2)
	# 		event_sub_types_batch.append(event_sub_types)
	# 		lexical_trigger_labels_batch.append(lexical_trigger_labels)
	# 		lexical_entity_labels_batch.append(lexical_entity_labels)


# 		# 处理最后不足一个batch的情况
# 		if len(event_type_batch) > 0:
# 			yield event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
# 					entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, event_sub_types_batch, lexical_trigger_labels_batch, lexical_entity_labels_batch

	def get_minibatch(self, batch_size):
		'''
		从数据集中获取minibatch
		Args:
			batch_size: number of sentences in a batch
		'''
		if self.shuffle:
			random.shuffle(self.data)		
		event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
			entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, \
			event_sub_types_batch, pos_tag_types_batch, lexical_trigger_labels_batch, lexical_entity_labels_batch \
			 = [], [], [], [], [], [], [], [], [], [], [], []
		for(event_type, argument_role, chars, entity_sub_types, entity_types, roles, distances_1, distances_2, event_sub_types, pos_tag_types, lexical_trigger_labels, lexical_entity_labels) in self.data:
			if len(event_type_batch) == batch_size:
				yield event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
					entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, \
					event_sub_types_batch, pos_tag_types_batch, lexical_trigger_labels_batch, lexical_entity_labels_batch
				event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
					entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, \
					event_sub_types_batch, pos_tag_types_batch, lexical_trigger_labels_batch, lexical_entity_labels_batch \
					 = [], [], [], [], [], [], [], [], [], [], [], []

			event_type_batch.append(event_type)
			argument_role_batch.append(argument_role)
			chars_batch.append(chars)
			entity_sub_types_batch.append(entity_sub_types)
			entity_types_batch.append(entity_types)
			roles_batch.append(roles)
			distances_1_batch.append(distances_1)
			distances_2_batch.append(distances_2)
			event_sub_types_batch.append(event_sub_types)
			pos_tag_types_batch.append(pos_tag_types)
			lexical_trigger_labels_batch.append(lexical_trigger_labels)
			lexical_entity_labels_batch.append(lexical_entity_labels)


		# 处理最后不足一个batch的情况
		if len(event_type_batch) > 0:
			yield event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
				entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, \
				event_sub_types_batch, pos_tag_types_batch, lexical_trigger_labels_batch, lexical_entity_labels_batch


if __name__ == '__main__':
	# devset = dataset('../../data/original/layer.6.5.4.3.2.1/dev.id.lm.full.txt')
	train_data_id_path = '../../Preprocess/data/processed/english/arguments/ltr/dev.id.txt'
	train_dataset = dataset(train_data_id_path, idlized = True, shuffle = False)
	# print(len(train_dataset))
	# for event in train_dataset:
	# 	print(event)
	# 	break
	# print(len(train_dataset))
	# print(train_dataset)

	i = 0
	for event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
		entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, event_sub_types_batch, \
		pos_tag_types_batch, lexical_trigger_labels_batch, lexical_entity_labels_batch in train_dataset.get_sentence_batch():

		# print(event_type_batch)
		# print(distances_1_batch)
		# print(entity_types_batch)
		# print(event_sub_types_batch)
		# print(event_type_batch)
		print(pos_tag_types_batch)
		# for roles in roles_batch:
		# 	print(roles)
		break

	# print(i)

		# print(chars_batch)
		# print(argument_role_batch)
		# break
		# entity_sub_types = entity_sub_types_batch[0]
		# print(entity_sub_types)
		# if len(set(entity_sub_types)) == 1:
		# 	print('没有实体信息')
		# 	print(chars_batch)
		# 	print(entity_sub_types)


