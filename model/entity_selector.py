#coding=utf-8
import sys
import os
sys.path.append(os.path.abspath('.'))
sys.path.append(os.path.abspath('..'))
from .base_model import *
import numpy as np
from .hyperparameter import *
import random


from configure import *

class entity_selector(base_model):
	"""
	用来做double loss的模型
	"""
	def __init__(self, hp, logger, vocabs, args, char_embeddings = None,word_embeddings = None):
		'''
		初始化
		Args:
			hp: 超参数
			logger: 日志
			vocabs: [1,2,3]字、标签、词的vocab
			args: 状态
			char_embeddings: 字向量
			word_embeddings: 词向量
		'''
		super(entity_selector, self).__init__(hp, logger)
		self.char_embeddings_vocab = char_embeddings
		self.word_embeddings_vocab = word_embeddings
		self.vocabs = vocabs

		self.char_vocab, self.event_type_vocab, self.event_sub_type_vocab, self.argument_role_vocab, self.argument_role_bio_vocab, \
			self.entity_type_vocab, self.entity_sub_type_vocab, self.pos_tag_vocab = vocabs
		self.args = args
		# self.hp.clip = 1

	def get_variables_for_training(self):
		'''
		返回需要优化的参数
		'''
		all_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)
		variables = []

		for var in all_vars:
			if 'selector' in str(var):
				# 先放入embedding类型的参数
				if 'embedding' in str(var):
					variables.append(var)
		for var in all_vars:
			if 'selector' in str(var):
				# 再放入非embedding类型的参数
				if 'embedding' not in str(var):
					variables.append(var)
		return variables

	# def print_variables(self,sess):
	# 	'''
	# 	打印参数
	# 	'''
	# 	all_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)
	# 	for var in all_vars:
	# 		if 'selector' not in str(var):
	# 			continue
	# 		self.logger.info(str(var))
	# 		self.logger.info(str(sess.run(var)))


	def build(self):
		'''
		构建可计算图
		'''
		with tf.variable_scope("selector", reuse = False):
			# 添加placeholders
			self.add_placeholders()
			# 添加向量化操作，得到每个词的向量表示
			self.add_word_embeddings_op()
			# 计算logits
			self.add_logits_op()
			# 计算概率
			self.add_pred_op()
			# 计算损失
			self.add_loss_op()

			# 提取运算变量
			self.variables = self.get_variables_for_training()

			# 定义训练操作
			# 这里的损失上乘以reward
			self.hp.clip = 1
			self.add_train_op(self.hp.optimizer, self.lr, self.loss,
				self.hp.clip)

			# 应用自己的梯度计算和梯度更新来覆盖前面的训练操作
			self.gradient_train_nodes()



			# 创建session和logger
			# self.initialize_session()

			# self.add_summary()


	def gradient_train_nodes(self):
		'''
		定义计算梯度以及梯度更新的运算
		'''
		# 需要在训练时填充
		self.input_gradients = []

		for param in self.variables:
			self.input_gradients.append(tf.placeholder(tf.float32, param.shape))
		self.apply_gradient_op = self.optimizer.apply_gradients(zip(self.input_gradients, self.variables))



	def add_placeholders(self):
		'''
		定义placeholder，所有的变量
		'''
		# 表示batch中每个句子的字的id表示
		# shape = (batch size, max length of sentence in batch)
		self.char_ids = tf.placeholder(tf.int32, shape = [None, None], name = "char_ids")

		# 表示batch中每个句子的字的pos tag表示
		# shape = (batch size, max length of sentence in batch)
		self.pos_tag_ids = tf.placeholder(tf.int32, shape = [None, None], name = "pos_tag_ids")

		# 表示batch中每个句子的长度
		# shape = (batch size,)
		self.sequence_lengths = tf.placeholder(tf.int32, shape = [None], name = "sequence_lengths")

		# 表示batch中每个句子的事件类型
		# shape = (batch size)
		self.event_type_labels = tf.placeholder(tf.int32, shape = [None], name = 'event_type_labels')

		# 表示batch中每个句子的论元类型
		# shape = (batch size)
		self.argument_role_labels = tf.placeholder(tf.int32, shape = [None], name = 'argument_role_labels')

		# 表示batch中每个句子的每个字的entity type
		# shape = (batch size, max length of sentence in batch)
		self.entity_type_labels = tf.placeholder(tf.int32, shape = [None, None], name = "entity_type_labels")

		# 表示batch中每个句子的每个字的entity sub type
		# shape = (batch size, max length of sentence in batch)
		self.entity_sub_type_labels = tf.placeholder(tf.int32, shape = [None, None], name = "entity_sub_type_labels")

		# 表示batch中每个句子的每个字的 event sub type
		# shape = (batch size, max length of sentence in batch)
		self.event_sub_type_labels = tf.placeholder(tf.int32, shape = [None, None], name = "event_sub_type_labels")

		# 表示batch中每个句子的每个字的argument role
		# shape = (batch size, max length of sentence in batch)
		self.argument_role_labels_per_token = tf.placeholder(tf.int32, shape = [None, None], name = "argument_role_labels_per_token")


		# 表示batch中每个句子的每个char的label
		# shape = (batch size, max length of sentence in batch, 2)
		self.labels = tf.placeholder(tf.float32, shape = [None, None], name = "labels")

		# dropout
		self.dropout = tf.placeholder(tf.float32, shape = [], name = "dropout")

		# 学习率
		self.lr = tf.placeholder(tf.float32, shape = [], name = "lr")

		# 表示输出的预测维度，应该是长度为1的向量，直接用标量表示被选中的概率
		self.output_dimension = 1

		# 表示返回的reward
		# self.reward = tf.placeholder(tf.float32, shape = [], name = "reward")

		# 表示哪些没有被Mask掉
		self.mask = tf.placeholder(tf.float32, shape = [None, None], name = "mask")

		self.dimension = 0

	def add_word_embeddings_op(self):
		'''
		添加embedding操作，包括词向量和字向量
		如果self.embeddings不是None，那么词向量就采用pre-trained vectors，否则自行训练
		字向量是自行训练的
		'''

		# 1. add char embedding
		self.logger.info("加入字向量")
		with tf.variable_scope("chars"):
			# 如果词向量是None
			if self.char_embeddings_vocab is None:
				self.logger.info("WARNING: randomly initializing word vectors")
				_char_embeddings = tf.get_variable(
					name = '_char_embeddings',
					dtype = tf.float32,
					shape = [len(self.char_vocab), self.hp.char_dimension]
					)
			else:
				# 加载已有的词向量
				_char_embeddings = tf.Variable(
					self.char_embeddings_vocab,
					name = '_char_embeddings',
					dtype = tf.float32,
					trainable = False
					)
				self._char_embeddings = _char_embeddings
			# lookup来获取word_ids对应的embeddings
			# shape = (batch size, max_length_sentence, dim)
			char_embeddings = tf.nn.embedding_lookup(
				_char_embeddings,
				self.char_ids,
				name = 'char_embeddings'
				)
			self.dimension += self.hp.char_dimension

		# 2. 加入entity type embedding
		self.logger.info('加入实体子类型向量')
		with tf.variable_scope("sub_entity"):
			self.logger.info("WARNING: randomly initializing word vectors")
			_sub_entity_embeddings = tf.get_variable(
				name = '_sub_entity_embeddings',
				dtype = tf.float32,
				shape = [len(self.entity_sub_type_vocab), self.hp.entity_subtype_embedding_dimension],
				trainable = True
				)
			# lookup来获取word_ids对应的embeddings
			# shape = (batch size, max_length_sentence, dim)
			sub_entity_embeddings = tf.nn.embedding_lookup(
				_sub_entity_embeddings,
				self.entity_sub_type_labels,
				name = 'sub_entity_embeddings'
				)
			char_embeddings = tf.concat([char_embeddings, sub_entity_embeddings], axis = -1)
			self.dimension += self.hp.entity_subtype_embedding_dimension

		# 3. 加入entity type embedding
		self.logger.info('加入实体类型向量')
		with tf.variable_scope("entity"):
			self.logger.info("WARNING: randomly initializing word vectors")
			_entity_embeddings = tf.get_variable(
				name = '_entity_embeddings',
				dtype = tf.float32,
				shape = [len(self.entity_type_vocab), self.hp.entity_type_embedding_dimension],
				trainable = True
				)
			# lookup来获取word_ids对应的embeddings
			# shape = (batch size, max_length_sentence, dim)
			entity_embeddings = tf.nn.embedding_lookup(
				_entity_embeddings,
				self.entity_type_labels,
				name = 'entity_embeddings'
				)
			char_embeddings = tf.concat([char_embeddings, entity_embeddings], axis = -1)
			self.dimension += self.hp.entity_type_embedding_dimension

		# 4. 加入event type embedding
		self.logger.info('加入事件子类型向量')
		with tf.variable_scope("event"):
			self.logger.info("WARNING: randomly initializing word vectors")
			_event_sub_type_embeddings = tf.get_variable(
				name = '_event_sub_type_embeddings',
				dtype = tf.float32,
				shape = [len(self.event_sub_type_vocab), self.hp.event_subtype_embedding_dimension],
				trainable = True
				)
			# lookup来获取word_ids对应的embeddings
			# shape = (batch size, max_length_sentence, dim)
			event_sub_type_embeddings = tf.nn.embedding_lookup(
				_event_sub_type_embeddings,
				self.event_sub_type_labels,
				name = 'event_sub_type_embeddings'
				)
			char_embeddings = tf.concat([char_embeddings, event_sub_type_embeddings], axis = -1)
			self.dimension += self.hp.event_subtype_embedding_dimension

		# # 5. 加入trigger的distance，用来对当前的trigger进行识别
		# self.logger.info('加入距离向量1')
		# with tf.variable_scope("distance_1"):
		# 	self.logger.info("WARNING: randomly initializing word vectors")
		# 	_distance_1_embeddings = tf.get_variable(
		# 		name = '_distance_1_embeddings',
		# 		dtype = tf.float32,
		# 		shape = [self.hp.distance_max - self.hp.distance_min, self.hp.distance_embedding_dimension],
		# 		trainable = True
		# 		)
		# 	# lookup来获取word_ids对应的embeddings
		# 	# shape = (batch size, max_length_sentence, dim)
		# 	distance_1_embeddings = tf.nn.embedding_lookup(
		# 		_distance_1_embeddings,
		# 		self.distance_1_labels,
		# 		name = 'distance_1_embeddings'
		# 		)
		# 	char_embeddings = tf.concat([char_embeddings, distance_1_embeddings], axis = -1)

		# 	self.dimension += self.hp.distance_embedding_dimension		

		# 6. 加入argument role 
		if self.hp.add_argument_role == 1:
			self.logger.info('加入论元类型向量')
			with tf.variable_scope("arguments"):
				self.logger.info("WARNING: randomly initializing word vectors")
				_argument_role_embeddings = tf.get_variable(
					name = '_argument_role_embeddings',
					dtype = tf.float32,
					shape = [len(self.argument_role_bio_vocab), self.hp.argument_role_embedding_dimension],
					trainable = True
					)
				# lookup来获取word_ids对应的embeddings
				# shape = (batch size, max_length_sentence, dim)
				argument_role_embeddings = tf.nn.embedding_lookup(
					_argument_role_embeddings,
					self.argument_role_labels_per_token,
					name = 'argument_role_embeddings'
					)
				char_embeddings = tf.concat([char_embeddings, argument_role_embeddings], axis = -1)
				self.dimension += self.hp.argument_role_embedding_dimension

		# 7. 加入pos tag type embedding
		if False:
			self.logger.info('加入pos tag embedding')
			with tf.variable_scope("pos_tag"):
				self.logger.info("WARNING: randomly initializing word vectors")
				_pos_tag_embeddings = tf.get_variable(
					name = '_pos_tag_embeddings',
					dtype = tf.float32,
					shape = [len(self.pos_tag_vocab), self.hp.pos_tag_embedding_dimension],
					trainable = True
					)
				# lookup来获取word_ids对应的embeddings
				# shape = (batch size, max_length_sentence, dim)
				pos_tag_embeddings = tf.nn.embedding_lookup(
					_pos_tag_embeddings,
					self.pos_tag_ids,
					name = 'pos_tag_embeddings'
					)
				char_embeddings = tf.concat([char_embeddings, pos_tag_embeddings], axis = -1)
				self.dimension += self.hp.pos_tag_embedding_dimension

		# shape = (batch size, max_length_sentence, dim)
		self.char_embeddings = char_embeddings

	def token_attention(self, lstm_outputs, name = None):
		"""
		建立attention机制，在lstm的输出进行
		Args:
			lstm_outputs:# shape = (batch size, max_length_sentence, hidden_size_lstm)
		"""
		with tf.variable_scope('dot_attention', reuse=tf.AUTO_REUSE):
			self.logger.info('加入了dot product attention')
			# shape = (batch size, hidden_size_lstm, max_length_sentence)
			lstm_outputs_t = tf.transpose(lstm_outputs,perm = [0,2,1])
			# shape = (batch size, max_length_sentence, max_length_sentence)
			att = tf.matmul(lstm_outputs, lstm_outputs_t)
			att_sm = tf.nn.softmax(att)
			# shape = (batch size, max_length_sentence, hidden_size_lstm)
			output = tf.matmul(att_sm,lstm_outputs)
			return output


		# with tf.variable_scope('tanh_attention', reuse = tf.AUTO_REUSE):
		# 	self.logger.info('加入了tanh attention')

		# 	W = tf.get_variable(
		# 				name = 'tanh_attention_w',
		# 				dtype = tf.float32,
		# 				shape = [2 * self.hp.hidden_size_lstm,2 * self.hp.hidden_size_lstm],
		# 				initializer = tf.contrib.layers.xavier_initializer()
		# 				)

		# 	batch_size = tf.shape(lstm_outputs)[0]
		# 	length = tf.shape(lstm_outputs)[1]
		# 	# shape = (batch size * max_length_sentence, hidden_size_lstm)
		# 	output_1 = tf.reshape(lstm_outputs, [batch_size * length, -1])
		# 	# shape = (batch size * max_length_sentence, hidden_size_lstm)
		# 	att = tf.matmul(output_1, W)
		# 	# shape = (batch size , max_length_sentence, hidden_size_lstm)
		# 	att = tf.reshape(att, [batch_size, length, -1])
		# 	# shape = (batch size, hidden_size_lstm, max_length_sentence)
		# 	lstm_outputs_t = tf.transpose(lstm_outputs, perm = [0,2,1])
		# 	# shape = (batch size, max_length_sentence, max_length_sentence)
		# 	att = tf.matmul(att, lstm_outputs_t)
		# 	# shape = (batch size, max_length_sentence, max_length_sentence)
		# 	# att = tf.nn.tanh(att)
		# 	att_sm = tf.nn.softmax(att)
		# 	# shape = (batch size, max_length_sentence, hidden_size_lstm)
		# 	output = tf.matmul(att_sm,lstm_outputs)

		# 	return output
		# return lstm_outputs

	def add_bi_lstm_layer(self, name, inputs):
		"""
		添加Bi-LSTM layer,并且控制是否添加Attention机制
		"""
		with tf.variable_scope(name):
			cell_fw = tf.contrib.rnn.LSTMCell(
				self.hp.hidden_size_lstm
				)
			cell_bw = tf.contrib.rnn.LSTMCell(
				self.hp.hidden_size_lstm
				)
			output = tf.nn.bidirectional_dynamic_rnn(
				cell_fw,
				cell_bw,
				inputs = inputs,
				sequence_length = self.sequence_lengths,
				dtype = tf.float32
				)
			# 取出output
			# shape = (batch size, max_length_sentence, hidden_size_lstm)
			(output_fw, output_bw), _ = output
			# shape = (batch size, max_length_sentence, 2 * hidden_size_lstm)
			output = tf.concat([output_fw, output_bw], axis = -1)
			# 进行attention操作
			# if self.args.attention:
			# 	output = self.token_attention(output)
			# 进行dropout
			# shape = (batch size, max_length_sentence, 2 * hidden_size_lstm)
			# output = tf.nn.dropout(output, self.dropout)
			return output


	def add_logits_op(self):
		'''
		定义self.logits，句子中的每个词都对应一个得分向量，维度是tags的维度
		'''

		# 首先对句子进行LSTM
		# shape = (batch size, max_length_sentence, 2 * hidden_size_lstm)
		output = self.add_bi_lstm_layer(
			name = 'bi-lstm',
			inputs = self.char_embeddings,
			)

		# 取出batch_size
		batch_size = tf.shape(output)[0]
		# 取出max_length_sentence
		nsteps = tf.shape(output)[1]

		# 在每个token的表示上添加event type embedding标示
		self.logger.info('加入事件类型向量')
		with tf.variable_scope('event_type'):
			self.logger.info("WARNING: randomly initializing word vectors")
			_event_type_embeddings = tf.get_variable(
				name = '_event_type_embeddings',
				dtype = tf.float32,
				shape = [len(self.event_type_vocab), self.hp.event_type_embedding_dimension],
				trainable = True
				)
			# lookup来获取word_ids对应的embeddings
			# shape = (batch size, dim)
			event_type_embeddings = tf.nn.embedding_lookup(
				_event_type_embeddings,
				self.event_type_labels,
				name = 'event_type_embeddings'
				)
			# repr_dimension += self.hp.event_type_embedding_dimension

		# 将(batch size, dim) 扩展到(batch size, max_length_sentence, dim)

		# 1. 先变换到(batch size, 1, dim)
		event_type_embeddings = tf.reshape(event_type_embeddings, 
			[batch_size, 1, self.hp.event_type_embedding_dimension])
		# 2. 复制得到（batch size, nsteps, dim)
		event_type_embeddings = tf.tile(event_type_embeddings,
			multiples = [1, nsteps, 1]
			)
		# 3. 进行合并
		# # shape = (batch size, max_length_sentence, 2 * hidden_size_lstm + dim)
		output = tf.concat([output, event_type_embeddings], axis = -1)

		# repr_dimension = tf.shape(output)[2]
		repr_dimension = self.hp.hidden_size_lstm * 2 + self.hp.event_type_embedding_dimension


		print(output.shape)

		# 然后用全联接网络计算概率
		with tf.variable_scope('proj'):
			W = tf.get_variable(
				name = 'w', 
				dtype = tf.float32,
				shape = [repr_dimension, self.output_dimension]
				)
			b = tf.get_variable(
				name = 'b',
				dtype = tf.float32,
				shape = [self.output_dimension],
				initializer = tf.zeros_initializer()
				)

			# shape = (batch size * max_length_sentence, dimension)
			output = tf.reshape(output, [-1, repr_dimension])

			# shape = (batch size * max_length_sentence, 1)
			pred = tf.matmul(output, W) + b

			# shape = (batch size, max_length_sentence)
			# 表示被选中的概率
			self.logits = tf.reshape(pred, [-1, nsteps])

		with tf.variable_scope('masked_softmax'):
			'''
			对logits进行masked softmax
			'''
			# shape = (batch size, max_length_sentence)
			self.masked_logits = self.logits + self.mask

			# 进行softmax操作
			# shape = (batch size, max_length_sentence)
			self.masked_logits = tf.nn.softmax(self.masked_logits)

			# 转换到3维
			# shape = (batch size, max_length_sentence, 1)
			self.masked_logits_2_dimension = tf.reshape(self.masked_logits, [batch_size, nsteps, -1])

			# 把softmax之后的结果展开，得到(0,1)的概率
			# shape = (batch size, max_length_sentence, 2)
			# 这个操作是错的
			self.masked_logits_2_dimension = tf.concat([1- self.masked_logits_2_dimension,self.masked_logits_2_dimension,], axis = -1)


	def add_pred_op(self):
		# 取出概率最大的维度的idx
		# shape = (batch size)
		if not self.hp.use_crf:
			self.labels_pred = tf.cast(tf.argmax(self.masked_logits, axis=-1), tf.int32)


	def add_loss_op(self):
		'''
		计算损失
		'''
		# 因为已经采用了softmax，因此需要自行计算损失
		# 取出batch_size
		batch_size = tf.shape(self.labels)[0]
		# 取出max_length_sentence
		nsteps = tf.shape(self.labels)[1]

		# 1. 将self.labels扩展为shape = (batch size, max_length_sentence, 2)
		labels_0_2_dim = tf.reshape(1 - self.labels, [batch_size, nsteps, 1])
		labels_1_2_dim = tf.reshape(self.labels, [batch_size, nsteps, 1])

		# 2. 进行concat
		labels_2_dim = tf.concat([labels_0_2_dim, labels_1_2_dim], axis = -1)

		# 3. 进行float32转换
		labels_2_dim = tf.to_float(labels_2_dim, name = 'ToFloat')

		# 4. 进行计算
		loss = labels_2_dim * tf.log(tf.clip_by_value(self.masked_logits_2_dimension, 1e-10, 1.0))

		# 5. 进行mask
		mask = tf.sequence_mask(self.sequence_lengths)
		loss = tf.boolean_mask(loss, mask)
		self.loss = -1 * tf.reduce_mean(loss)

		# losses = tf.nn.sparse_softmax_cross_entropy_with_logits(
		# 	logits = self.masked_logits_2_dimension,
		# 	labels = self.labels
		# 	)
		# mask = tf.sequence_mask(self.sequence_lengths)
		# losses = tf.boolean_mask(losses, mask)
		# self.loss = tf.reduce_mean(losses)


		# if True:
		# 	lambd = 0.0001
		# 	self.logger.info('添加了l2正则化,系数为' + str(lambd))

		# 	tv = self.get_variables_for_training()
		# 	regularization_cost = lambd* tf.reduce_sum([ tf.nn.l2_loss(v) for v in tv ])
		# 	self.loss += regularization_cost


		tf.summary.scalar('loss', self.loss)


	def pad_sequences(self, sequences, pad_token, nlevels = 1):
		'''
		对sequence进行填充
		Args:
			sequences: a generator of list or tuple
			pad_token: the token to pad with
			nlevels: padding的深度，如果是1，则表示对词进行填充，如果是2表示对字进行填充
		Return:
			a list of list where each sublist has same length
		'''
		# print '--------pad-sequences--------'
		if nlevels == 1:
			# 找到sequences中的句子最大长度
			max_length_sentence = max(map(lambda x: len(x), sequences))
			# 然后直接进行padding
			sequences_padded, sequences_length = self._pad_sequences(sequences, 
				pad_token, max_length_sentence)

		if nlevels == 2:
			# 找到sequence中所有句子的所有单词中字母数最大的单词
			max_length_word = max([max(map(lambda x: len(x), seq)) for seq in sequences])
			# print max_length_word
			sequences_padded, sequences_length = [], []
			for seq in sequences:
				# print seq
				# 将每个句子的每个词都进行填充
				sp, sl = self._pad_sequences(seq, pad_token, max_length_word)
				# print sp, sl
				# 每个句子的字母的表示
				sequences_padded += [sp]
				# 每个句子的字母的长度
				sequences_length += [sl]
			# 然后对句子进行填充
			# batch中最大长度的句子
			max_length_sentence = max(map(lambda x : len(x), sequences))
			# 填充的时候用[0,0,0,0,0]用字母向量进行填充
			sequences_padded, _ = self._pad_sequences(sequences_padded, 
				[pad_token] * max_length_word, max_length_sentence)
			# 得到句子的每个单词的字母的长度 (batch, max_length_sentence, letter_length)
			sequences_length, _ = self._pad_sequences(sequences_length, 0, max_length_sentence)



		return sequences_padded, sequences_length



	def _pad_sequences(self, sequences, pad_token, max_length):
		'''
		对sequences进行填充
		Args:
			pad_token: the token to pad with
		'''
		sequences_padded, sequences_lengths = [], []
		for sequence in sequences:
			sequence = list(sequence)
			# 获取句子长度
			sequences_lengths += [min(len(sequence), max_length)]
			# 进行填充
			sequence = sequence[:max_length] + [pad_token] * max(max_length - len(sequence), 0)
			sequences_padded += [sequence]
		return sequences_padded, sequences_lengths

	def get_feed_dict(self, event_type_batch, event_sub_types_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
		entity_types_batch, roles_batch, mask, pos_tag_types_batch, labels = None, reward = None, lr = None, dropout = None):
		'''
		Args:
			lr: learning rate
			dropout: keep prob
		'''
		padding_idx = -1
		chars_batch_, sequence_lengths = self.pad_sequences(chars_batch, padding_idx)
		entity_sub_types_batch_, _ = self.pad_sequences(entity_sub_types_batch, padding_idx)
		event_sub_types_batch_, _ = self.pad_sequences(event_sub_types_batch, padding_idx)
		entity_types_batch_, _ = self.pad_sequences(entity_types_batch, padding_idx)
		roles_batch_, _ = self.pad_sequences(roles_batch, padding_idx)
		pos_tag_types_batch_, _ = self.pad_sequences(pos_tag_types_batch, padding_idx)

		feed = {
			self.char_ids: chars_batch_, # 句子的词的id表示(batch, max_length_sentence)
			self.sequence_lengths: sequence_lengths, # 句子的词的长度(batch, )
			self.event_type_labels: event_type_batch,
			self.argument_role_labels : argument_role_batch,
			self.entity_type_labels : entity_types_batch_,
			self.entity_sub_type_labels: entity_sub_types_batch_,
			self.event_sub_type_labels: event_sub_types_batch_,
			self.argument_role_labels_per_token:roles_batch_,
			self.pos_tag_ids: pos_tag_types_batch_
		}

		if mask is not None:
			mask_, _ = self.pad_sequences(mask, float("-inf"))
			feed[self.mask] = mask_

		if labels is not None:
			labels_, _ = self.pad_sequences(labels, 0)
			feed[self.labels] = labels_

		if reward is not None:
			feed[self.reward] = reward

		# 需要构造mask给softmax之前

		if lr is not None:
			feed[self.lr] = lr
		if dropout is not None:
			feed[self.dropout] = dropout
		
		return feed, sequence_lengths


	def run_evaluate(self, test, output_all_file = None, output_wrong_file = None, output_class_wrong_file = None):
		'''
		在测试集上运行，并且统计结果，包括precision/recall/accuracy/f1
		Args:
			test: 一个dataset instance
		Returns:
			metrics: dict metrics['acc'] = 98.4
		'''
		accs = []
		# 进行identification的记录
		correct_preds_id, total_correct_id, total_preds_id = 0., 0., 0. 
		# 进行classification的记录
		correct_preds_cl, total_correct_cl, total_preds_cl = 0., 0., 0.
		
		idx_to_tag = {idx : tag for tag, idx in self.vocab_tags.items()}
		idx_to_token = {idx: token for token, idx in self.vocab_chars.items()}
		# chars_batch, labels_batch, _, _, entity_sub_types_batch, entity_types_batch
		for words, labels, _, _, entity_sub_types_batch, entity_types_batch in test.get_minibatch_with_lm_embedding(self.hp.batch_size):
			# predict_batch
			# shape = (batch size, max_length_sentence)
			# shape = (batch size,)
			idx = -1 # 表示这个batch中句子的index
			labels_pred, sequence_lengths, logits_sequences = self.predict_batch(words, entity_sub_types_batch, entity_types_batch)

			for lab, lab_pred, length, logits in zip(labels, labels_pred, sequence_lengths, logits_sequences):
				idx += 1
				# 取每一句话，长度为length
				# 正确label
				lab = lab[:length]
				# 预测得到的label
				lab_pred = lab_pred[:length]


				# 预测正确的个数
				accs += [a == b for (a, b) in zip(lab, lab_pred)]

				# 真实chunk
				lab_chunks = set(self.get_chunks(lab, self.vocab_tags))
				# 预测chunk
				lab_pred_chunks = set(self.get_chunks(lab_pred, self.vocab_tags))

				# 1. 计算identification效果

				# 真实结果
				chunks_without_label = set([(item[1], item[2]) for item in lab_chunks])
				# 预测结果
				chunks_pred_without_label = set([(item[1], item[2]) for item in lab_pred_chunks])
				# 记录正确的chunk数量
				correct_preds_id += len(chunks_without_label & chunks_pred_without_label)
				# 预测出的chunk数量
				total_preds_id += len(chunks_pred_without_label)
				# 正确的chunk数量
				total_correct_id += len(chunks_without_label)

				# 2. 计算classification效果

				# 记录正确的chunk数量
				correct_preds_cl += len(lab_chunks & lab_pred_chunks)
				# 预测出的chunk数量
				total_preds_cl += len(lab_pred_chunks)
				# 正确的chunk数量
				total_correct_cl += len(lab_chunks)

				# 3. 进行输出
				# 3.1 输出所有结果
				line = words[idx]
				if output_all_file != None:
					for char,tag, tag_pred, logit in zip(line,lab, lab_pred, logits):
						token = idx_to_token[char]
						tag = idx_to_tag[tag]
						tag_pred = idx_to_tag[tag_pred]
						output_all_file.write(token + '	' + tag + '	' + tag_pred + '\n')
						output_all_file.write(','.join([str(x) for x in logit])+'\n')
					output_all_file.write('\n')
				# 3.2 输出错误结果
				if output_wrong_file != None and lab != lab_pred:
					for char,tag, tag_pred in zip(line,lab, lab_pred):
						token = idx_to_token[char]
						tag = idx_to_tag[tag]
						tag_pred = idx_to_tag[tag_pred]
						output_wrong_file.write(token + '	' + tag + '	' + tag_pred + '\n')
					output_wrong_file.write('\n')

				# 3.3 输出识别正确但是分类错误的结果
				if output_class_wrong_file != None and len(chunks_without_label & chunks_pred_without_label) != len(lab_chunks & lab_pred_chunks):
					for char,tag, tag_pred in zip(line,lab, lab_pred):
						token = idx_to_token[char]
						tag = idx_to_tag[tag]
						tag_pred = idx_to_tag[tag_pred]
						output_class_wrong_file.write(token + '	' + tag + '	' + tag_pred + '\n')
					output_class_wrong_file.write('\n')

				# # 4 输出logits
				# if output_all_file != None:
				# 	output_all_file.write(logits+'\n')

		# 1. 计算trigger identification的结果
		# 计算precision，预测出的chunk中有多少是正确的
		p1 = correct_preds_id / total_preds_id if correct_preds_id > 0 else 0
		# 计算recall，预测正确的chunk占了所有chunk的数量
		r1 = correct_preds_id / total_correct_id if correct_preds_id > 0 else 0
		# 计算f1
		f1_1 = 2 * p1 * r1 / (p1 + r1) if correct_preds_id > 0 else 0

		# 2. 计算trigger classification的结果
		# 计算precision，预测出的chunk中有多少是正确的
		p2 = correct_preds_cl / total_preds_cl if correct_preds_cl > 0 else 0
		# 计算recall，预测正确的chunk占了所有chunk的数量
		r2 = correct_preds_cl / total_correct_cl if correct_preds_cl > 0 else 0
		# 计算f1
		f1_2 = 2 * p2 * r2 / (p2 + r2) if correct_preds_cl > 0 else 0
		# 计算accuracy，用预测对的词的比例来进行表示
		acc = np.mean(accs)

		# 返回结果
		return {
			'p1': 100 * p1,
			'r1': 100 * r1,
			'f1_1': 100 * f1_1,
			'p2': 100 * p2,
			'r2': 100 * r2,
			'f1_2': 100 * f1_2
		}

	def calculate_gradient(self, sess, event_type_batch, event_sub_types_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
		entity_types_batch, roles_batch, mask, labels, pos_tag_types_batch):
		'''
		根据当前结果计算梯度，并且返回梯度
		'''
		# 构造数据
		fd, sequence_lengths = self.get_feed_dict(event_type_batch, event_sub_types_batch, \
			argument_role_batch, chars_batch, entity_sub_types_batch,\
			entity_types_batch, roles_batch, mask, pos_tag_types_batch, \
			labels, lr = self.hp.learning_rate, dropout = 1.0)

		grads = sess.run(
			self.grads,
			feed_dict = fd
			)
		return grads

	def apply_gradient(self, sess, model_gradients):
		'''
		根据已经计算出的梯度，对参数进行更新
		Args:
			model_gradients: [model_gradient],每个元素是
		'''

		feed_dict = {}
		# 所有参数的梯度填充placeholder
		for i in range(len(self.variables)):
			feed_dict[self.input_gradients[i]] = model_gradients[i]
		feed_dict[self.lr] = self.hp.learning_rate
		# 进行梯度更新
		sess.run(self.apply_gradient_op, feed_dict = feed_dict)


	def run_train_op(self, sess, event_type_batch, event_sub_types_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
		entity_types_batch, roles_batch, mask, reward, labels):
		'''
		根据给定的结果进行预测
		Args:
			words: list of sentences
		Returns:
			labels_pred: list of labels for each sentence
		'''
		fd, sequence_lengths = self.get_feed_dict(event_type_batch, event_sub_types_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
		entity_types_batch, roles_batch, mask, labels, reward, lr = self.hp.learning_rate, dropout = 1.0)

		# 执行计算
		_, train_loss = sess.run(
			[self.train_op, self.loss],
			feed_dict = fd
			)
		# self.print_variables(sess)
		self.logger.info('loss is ' + str(train_loss))


	def predict_batch(self, sess, event_type_batch, event_sub_types_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
		entity_types_batch, roles_batch, mask, pos_tag_types_batch):
		'''
		对一个batch进行预测，并返回预测结果
		Args:
			words: list of sentences
		Returns:
			labels_pred: list of labels for each sentence
		'''
		logits_sequences = []
		# chars_batch, segs_batch, words_batch, lm_embeddings_batch, labels_batch
		fd, sequence_lengths = self.get_feed_dict(event_type_batch, event_sub_types_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
		entity_types_batch, roles_batch, mask, pos_tag_types_batch, lr = self.hp.learning_rate, dropout = 1.0)

		# shape = (batch size,)
		# print(sess.run([self.logits,self.masked_logits], feed_dict = fd))
		labels_pred = sess.run(self.labels_pred, feed_dict = fd)

		return labels_pred, sequence_lengths

	def predict_batch_and_sample(self, sess, event_type_batch, event_sub_types_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
		entity_types_batch, roles_batch, mask, pos_tag_types_batch,):
		'''
		对一个batch进行预测，并且对结果进行sample
		Args:
			words: list of sentences
		Returns:
			labels_pred: list of labels for each sentence
		'''
		logits_sequences = []
		# self.logger.debug(str(mask))
		# chars_batch, segs_batch, words_batch, lm_embeddings_batch, labels_batch
		fd, sequence_lengths = self.get_feed_dict(event_type_batch, event_sub_types_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
		entity_types_batch, roles_batch, mask, pos_tag_types_batch, lr = self.hp.learning_rate, dropout = 1.0)

		# shape = (batch size, max_length_sentence)
		masked_logits = sess.run(self.masked_logits, feed_dict = fd)

		# 对masked logits进行sample
		# shape = (batch size)
		labels_sampled = []

		for masked_logits_for_each_sentence in masked_logits:
			# 用random给出一个[0,1]之间的数字，然后看这个数字在哪个idx上
			count = 0.0
			number = random.random()
			# self.logger.debug(str(number))
			# self.logger.debug(', '.join([str(x.item()) for x in masked_logits_for_each_sentence]))
			for idx, logit in enumerate(masked_logits_for_each_sentence):
				count += logit.item()
				if count > number:
					labels_sampled.append(idx)
					# self.logger.debug(str(count))
					# self.logger.debug(str(idx))
					break

		return labels_sampled, sequence_lengths


	def get_chunks(self, seq, tags):
		'''
		给定一个序列的tags，将其中的entity和位置取出来
		Args:
			seq: [4,4,0,0,1,2...] 一个句子的label
			tags: dict['I-LOC'] = 2
		Returns:
			list of (chunk_type, chunk_start, chunk_end)

		Examples:
			seq = [4, 5, 0, 3]
			tags = {
			'B-PER' : 4,
			'I-PER' : 5,
			'B-LOC' : 3
			'O' : 0
			}

			Returns:
				chunks = [
					('PER', 0, 2),
					('LOC', 3, 4)
				]
		'''
		idx_to_tag = {idx : tag for tag, idx in tags.items()}
		# print idx_to_tag
		chunks = []

		# 表示当前的chunk的起点和类型
		chunk_start, chunk_type = None, None
		# print seq

		for i, tag_idx in enumerate(seq):
			# 如果不是entity的一部分
			if tag_idx == tags['O']:
				# 如果chunk_type不是None，那么就是一个entity的结束
				if chunk_type != None:
					chunk = (chunk_type, chunk_start, i)
					chunks.append(chunk)
					chunk_start, chunk_type = None, None
				# 如果chunk_type是None，那么就不需要处理
				else:
					pass
			# 如果是BI
			else:
				tag = idx_to_tag[tag_idx]
				# 如果是B
				if tag[0] == 'B':
					# 如果前面有entity，那么这个entity就完成了
					if chunk_type != None:
						chunk = (chunk_type, chunk_start, i)
						chunks.append(chunk)
						chunk_start, chunk_type = None, None

					# 记录开始
					chunk_start = i
					chunk_type = tag[2:]

				# 如果是I
				else:
					if chunk_type != None:
						# 如果chunk_type发生了变化，例如(B-PER, I-PER, I-LOC)，那么就需要将(B-PER, I-PER)归类为chunk
						if chunk_type != tag[2:]:
							chunk = (chunk_type, chunk_start, i)
							chunks.append(chunk)
							chunk_start, chunk_type = None, None
		
		# 处理可能存在的最后一个未结尾的chunk
		if chunk_type != None:
			chunk = (chunk_type, chunk_start, i + 1)
			chunks.append(chunk)
		return chunks

if __name__ == '__main__':
	model = seq_tag_model(None, None, [None,None,None], None)
	seq = [4, 4, 5, 0, 3, 5]
	tags = {
			'B-PER' : 4,
			'I-PER' : 5,
			'B-LOC' : 3,
			'O' : 0
			}
	print(model.get_chunks(seq, tags))

