#coding=utf-8
from .base_model import *
import numpy as np
from .hyperparameter import *
import sys
sys.path.append(os.path.abspath('.'))
from EnglishArgumentDetectionModels.configure import *
from .util import *
from .vocab import *
from tqdm import tqdm
import math

class cnn_model(base_model):
	"""
	要么不用，要么全都用
	"""
	def __init__(self, hp, logger, vocabs, args, char_embeddings = None,word_embeddings = None):
		'''
		初始化
		Args:
			hp: 超参数
			logger: 日志
			vocabs: [char_vocab, event_type_vocab, argument_role_vocab, entity_type_vocab, entity_sub_type_vocab]
			args: 状态
			entity_selector: 表示
			char_embeddings: 字向量
			word_embeddings: 词向量
		'''
		super(cnn_model, self).__init__(hp, logger)
		self.char_embeddings_vocab = char_embeddings
		self.vocabs = vocabs

		self.char_vocab, self.event_type_vocab, self.event_sub_type_vocab, self.argument_role_vocab, self.argument_role_bio_vocab, self.entity_type_vocab, \
			self.entity_sub_type_vocab, self.pos_tag_vocab = vocabs
		self.args = args

		self.idx_to_argument_role_tag = id_to_vocab(self.argument_role_vocab) # id to vocab
		self.idx_to_argument_role_bio_tag = id_to_vocab(self.argument_role_bio_vocab)# id to vocab
		self.dev_instance_num = 0

		self.sess = None
		self.saver = None

		self.stage = 0 # 0 表示预训练状态 # 1 表示联合训练状态
		self.selector = None

	def get_variables_for_training(self):
		'''

		返回需要优化的参数
		'''
		all_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES)
		return all_vars
		# variables = []
		# for var in all_vars:
		# 	if 'detector' in str(var):
		# 		variables.append(var)
		# return variables

	def build(self):
		'''
		构建可计算图
		'''
		with tf.variable_scope("detector"):
			# 添加placeholders
			self.add_placeholders()
			# 添加向量化操作，得到每个词的向量表示
			self.add_word_embeddings_op()
			# 计算logits
			self.add_logits_op()
			# 计算概率
			self.add_pred_op()
			# 计算损失
			self.add_loss_op()

			# 定义训练操作
			self.add_train_op(self.hp.optimizer, self.lr, self.loss,
				self.hp.clip)
			# 创建session和logger
			# self.initialize_session()



	def add_placeholders(self):
		'''
		定义placeholder，所有的变量
		'''
		# 表示batch中每个句子的字的id表示
		# shape = (batch size, max length of sentence in batch)
		self.char_ids = tf.placeholder(tf.int32, shape = [None, None], name = "char_ids")

		# 表示batch中每个句子的字的pos tag表示
		# shape = (batch size, max length of sentence in batch)
		self.pos_tag_ids = tf.placeholder(tf.int32, shape = [None, None], name = "pos_tag_ids")

		# 表示batch中每个句子的长度
		# shape = (batch size,)
		self.sequence_lengths = tf.placeholder(tf.int32, shape = [None], name = "sequence_lengths")

		# 表示batch中每个句子的事件类型
		# shape = (batch size)
		self.event_type_labels = tf.placeholder(tf.int32, shape = [None], name = 'event_type_labels')

		# 表示batch中每个句子的论元类型
		# shape = (batch size)
		self.argument_role_labels = tf.placeholder(tf.int32, shape = [None], name = 'argument_role_labels')

		# 表示batch中每个句子的每个字的entity type
		# shape = (batch size, max length of sentence in batch)
		self.entity_type_labels = tf.placeholder(tf.int32, shape = [None, None], name = "entity_type_labels")

		# 表示batch中每个句子的每个字的entity sub type
		# shape = (batch size, max length of sentence in batch)
		self.entity_sub_type_labels = tf.placeholder(tf.int32, shape = [None, None], name = "entity_sub_type_labels")

		# 表示batch中每个句子的每个字的 event sub type
		# shape = (batch size, max length of sentence in batch)
		self.event_sub_type_labels = tf.placeholder(tf.int32, shape = [None, None], name = "event_sub_type_labels")

		# 表示batch中每个句子的每个字的argument role
		# shape = (batch size, max length of sentence in batch)
		self.argument_role_labels_per_token = tf.placeholder(tf.int32, shape = [None, None], name = "argument_role_labels_per_token")

		# 表示batch中每个句子的每个字的distance 1
		# shape = (batch size, max length of sentence in batch)
		self.distance_1_labels = tf.placeholder(tf.int32, shape = [None, None], name = "distance_1_labels")

		# 表示batch中每个句子的每个字的distance 2
		# shape = (batch size, max length of sentence in batch)
		self.distance_2_labels = tf.placeholder(tf.int32, shape = [None, None], name = "distance_2_labels")

		# 添加lexical features
		# shape = (batch size, length)
		self.lexical_trigger_labels = tf.placeholder(tf.int32, shape = [None, None], name = "lexical_trigger_labels")
		self.lexical_entity_labels = tf.placeholder(tf.int32, shape = [None, None], name = "lexical_entity_labels")

		# dropout
		self.dropout = tf.placeholder(tf.float32, shape = [], name = "dropout")
		# 学习率
		self.lr = tf.placeholder(tf.float32, shape = [], name = "lr")

		self.dimension = 0




	def add_word_embeddings_op(self):
		'''
		添加embedding操作，包括词向量和字向量
		如果self.embeddings不是None，那么词向量就采用pre-trained vectors，否则自行训练
		字向量是自行训练的
		'''
		# 1. add char embedding
		self.logger.info("加入字向量")
		with tf.variable_scope("chars"):
			# 如果词向量是None
			if self.char_embeddings_vocab is None:
				self.logger.info("WARNING: randomly initializing word vectors")
				_char_embeddings = tf.get_variable(
					name = '_char_embeddings',
					dtype = tf.float32,
					shape = [len(self.char_vocab), self.hp.char_dimension]
					)
			else:
				# 加载已有的词向量
				_char_embeddings = tf.Variable(
					self.char_embeddings_vocab,
					name = '_char_embeddings',
					dtype = tf.float32,
					trainable = False
					)
				self._char_embeddings = _char_embeddings
			# lookup来获取word_ids对应的embeddings
			# shape = (batch size, max_length_sentence, dim)
			char_embeddings = tf.nn.embedding_lookup(
				_char_embeddings,
				self.char_ids,
				name = 'char_embeddings'
				)

			self.dimension += self.hp.char_dimension

		# 2. 加入position embedding
		self.logger.info('加入距离向量1')
		with tf.variable_scope("distance_1"):
			self.logger.info("WARNING: randomly initializing word vectors")
			_distance_1_embeddings = tf.get_variable(
				name = '_distance_1_embeddings',
				dtype = tf.float32,
				shape = [self.hp.distance_max - self.hp.distance_min, self.hp.distance_embedding_dimension],
				trainable = True
				)
			# lookup来获取word_ids对应的embeddings
			# shape = (batch size, max_length_sentence, dim)
			distance_1_embeddings = tf.nn.embedding_lookup(
				_distance_1_embeddings,
				self.distance_1_labels,
				name = 'distance_1_embeddings'
				)
			char_embeddings = tf.concat([char_embeddings, distance_1_embeddings], axis = -1)

			self.dimension += self.hp.distance_embedding_dimension

		# 3. 加入position embedding
		self.logger.info('加入距离向量2')
		with tf.variable_scope("distance_2"):
			self.logger.info("WARNING: randomly initializing word vectors")
			_distance_2_embeddings = tf.get_variable(
				name = '_distance_2_embeddings',
				dtype = tf.float32,
				shape = [self.hp.distance_max - self.hp.distance_min, self.hp.distance_embedding_dimension],
				trainable = True
				)
			# lookup来获取word_ids对应的embeddings
			# shape = (batch size, max_length_sentence, dim)
			distance_2_embeddings = tf.nn.embedding_lookup(
				_distance_2_embeddings,
				self.distance_2_labels,
				name = 'distance_2_embeddings'
				)
			char_embeddings = tf.concat([char_embeddings, distance_2_embeddings], axis = -1)
			self.dimension += self.hp.distance_embedding_dimension

		# 4. 加入entity type embedding
		# self.logger.info('加入实体子类型向量')
		# with tf.variable_scope("sub_entity"):
		# 	self.logger.info("WARNING: randomly initializing word vectors")
		# 	_sub_entity_embeddings = tf.get_variable(
		# 		name = '_sub_entity_embeddings',
		# 		dtype = tf.float32,
		# 		shape = [len(self.entity_sub_type_vocab), self.hp.entity_subtype_embedding_dimension],
		# 		trainable = True
		# 		)
		# 	# lookup来获取word_ids对应的embeddings
		# 	# shape = (batch size, max_length_sentence, dim)
		# 	sub_entity_embeddings = tf.nn.embedding_lookup(
		# 		_sub_entity_embeddings,
		# 		self.entity_sub_type_labels,
		# 		name = 'sub_entity_embeddings'
		# 		)
		# 	char_embeddings = tf.concat([char_embeddings, sub_entity_embeddings], axis = -1)
		# 	self.dimension += self.hp.entity_subtype_embedding_dimension

		# 5. 加入entity type embedding
		self.logger.info('加入实体类型向量')
		with tf.variable_scope("entity"):
			self.logger.info("WARNING: randomly initializing word vectors")
			_entity_embeddings = tf.get_variable(
				name = '_entity_embeddings',
				dtype = tf.float32,
				shape = [len(self.entity_type_vocab), self.hp.entity_type_embedding_dimension],
				trainable = True
				)
			# lookup来获取word_ids对应的embeddings
			# shape = (batch size, max_length_sentence, dim)
			entity_embeddings = tf.nn.embedding_lookup(
				_entity_embeddings,
				self.entity_type_labels,
				name = 'entity_embeddings'
				)
			char_embeddings = tf.concat([char_embeddings, entity_embeddings], axis = -1)
			self.dimension += self.hp.entity_type_embedding_dimension

		# 6. 加入entity type embedding
		# if False:
		self.logger.info('加入事件子类型向量')
		with tf.variable_scope("event"):
			self.logger.info("WARNING: randomly initializing word vectors")
			_event_sub_type_embeddings = tf.get_variable(
				name = '_event_sub_type_embeddings',
				dtype = tf.float32,
				shape = [len(self.event_sub_type_vocab), self.hp.event_subtype_embedding_dimension],
				trainable = True
				)
			# lookup来获取word_ids对应的embeddings
			# shape = (batch size, max_length_sentence, dim)
			event_sub_type_embeddings = tf.nn.embedding_lookup(
				_event_sub_type_embeddings,
				self.event_sub_type_labels,
				name = 'event_sub_type_embeddings'
				)
			char_embeddings = tf.concat([char_embeddings, event_sub_type_embeddings], axis = -1)
			self.dimension += self.hp.event_subtype_embedding_dimension

		# 6. 加入argument role 
		if self.hp.add_argument_role == 1:
			self.logger.info('加入论元类型向量')
			with tf.variable_scope("arguments"):
				self.logger.info("WARNING: randomly initializing word vectors")
				_argument_role_embeddings = tf.get_variable(
					name = '_argument_role_embeddings',
					dtype = tf.float32,
					shape = [len(self.argument_role_bio_vocab), self.hp.argument_role_embedding_dimension],
					trainable = True
					)
				# lookup来获取word_ids对应的embeddings
				# shape = (batch size, max_length_sentence, dim)
				argument_role_embeddings = tf.nn.embedding_lookup(
					_argument_role_embeddings,
					self.argument_role_labels_per_token,
					name = 'argument_role_embeddings'
					)
				char_embeddings = tf.concat([char_embeddings, argument_role_embeddings], axis = -1)
				self.dimension += self.hp.argument_role_embedding_dimension


		# 7. 加入pos tag type embedding
		if False:
			self.logger.info('加入pos tag embedding')
			with tf.variable_scope("pos_tag"):
				self.logger.info("WARNING: randomly initializing word vectors")
				_pos_tag_embeddings = tf.get_variable(
					name = '_pos_tag_embeddings',
					dtype = tf.float32,
					shape = [len(self.pos_tag_vocab), self.hp.pos_tag_embedding_dimension],
					trainable = True
					)
				# lookup来获取word_ids对应的embeddings
				# shape = (batch size, max_length_sentence, dim)
				pos_tag_embeddings = tf.nn.embedding_lookup(
					_pos_tag_embeddings,
					self.pos_tag_ids,
					name = 'pos_tag_embeddings'
					)
				char_embeddings = tf.concat([char_embeddings, pos_tag_embeddings], axis = -1)
				self.dimension += self.hp.pos_tag_embedding_dimension

		# # shape = (batch size, max_length_sentence, dim)
		self.char_embeddings = tf.nn.dropout(char_embeddings, self.dropout)

		# self.char_embeddings = char_embeddings

		# self.char_embedding_dimension = self.hp.char_dimension + 2 * self.hp.distance_embedding_dimension \
		# 	+ self.hp.entity_subtype_embedding_dimension + self.hp.argument_role_embedding_dimension


	def add_logits_op(self):
		'''
		定义self.logits，句子中的每个词都对应一个得分向量，维度是tags的维度
		'''
		repr_dimension = 0
		# 首先对句子进行卷积
		with tf.variable_scope('cnn'):
			# shape (batch size, max sentence length, kernel nums)
			feature_maps = tf.layers.conv1d(
				inputs = self.char_embeddings,
				kernel_size = self.hp.cnn_kernel_size,
				filters = self.hp.cnn_kernel_nums,
				padding = 'valid',
				use_bias = True,
				activation = tf.nn.tanh,
				name = 'convolution'
				)
		print(feature_maps.shape)
		# 进行pooling
		with tf.variable_scope('poolinig'):
			output = tf.reduce_max(feature_maps, reduction_indices=[1], name='max_pooling')
			repr_dimension = self.hp.cnn_kernel_nums

		# 添加lexical features
		with tf.variable_scope('lexical_level_features'):
			sequenth_length = tf.shape(self.lexical_trigger_labels)[1]
			lexical_trigger_embedding = tf.nn.embedding_lookup(
				self._char_embeddings,
				self.lexical_trigger_labels,
				name = 'lexical_trigger_embedding'
				)
			# shape = (batch, None, dim)
			lexical_entity_embedding = tf.nn.embedding_lookup(
				self._char_embeddings,
				self.lexical_trigger_labels,
				name = 'lexical_entity_embedding'
				)

			# shape = (batch, dim)
			lexical_trigger_repr = tf.reduce_sum(
				input_tensor = lexical_trigger_embedding,
				axis = 1
				)
			# shape = (batch, dim)
			lexical_entity_repr = tf.reduce_sum(
				input_tensor = lexical_entity_embedding,
				axis = 1
				)
			repr_dimension += 2 * self.hp.char_dimension
		output = tf.concat([output, lexical_trigger_repr, lexical_entity_repr], axis = -1)


		# 添加事件type embedding
		self.logger.info('加入事件类型向量')
		with tf.variable_scope('event_type'):
			self.logger.info("WARNING: randomly initializing word vectors")
			_event_type_embeddings = tf.get_variable(
				name = '_event_type_embeddings',
				dtype = tf.float32,
				shape = [len(self.event_type_vocab), self.hp.event_type_embedding_dimension],
				trainable = True
				)
			# lookup来获取word_ids对应的embeddings
			# shape = (batch size, dim)
			event_type_embeddings = tf.nn.embedding_lookup(
				_event_type_embeddings,
				self.event_type_labels,
				name = 'event_type_embeddings'
				)
			repr_dimension += self.hp.event_type_embedding_dimension
		# shape = (batch size, dimension + event_type_embedding_dimension)
		output = tf.concat([event_type_embeddings, output], axis = -1)


		# 然后用全联接网络计算概率
		with tf.variable_scope('proj'):
			W = tf.get_variable(
				name = 'w', 
				dtype = tf.float32,
				shape = [repr_dimension, len(self.argument_role_vocab)]
				)
			b = tf.get_variable(
				name = 'b',
				dtype = tf.float32,
				shape = [len(self.argument_role_vocab)],
				initializer = tf.zeros_initializer()
				)
			# shape = (batch size, vocab_tags_nums)
			# self.logits = tf.tanh(tf.matmul(output, W) + b)
			self.logits = tf.matmul(output, W) + b

	def add_pred_op(self):
		'''
		计算prediction，如果使用crf的话，需要
		'''
		# 取出概率最大的维度的idx
		
		# if not self.hp.use_crf:
		# shape = (batch size,)
		self.labels_pred = tf.cast(tf.argmax(self.logits, axis=-1), tf.int32)

	def add_loss_op(self):
		'''
		计算损失
		'''
		# shape = (batch size)
		losses = tf.nn.sparse_softmax_cross_entropy_with_logits(
			logits = self.logits,
			labels = self.argument_role_labels
			)
		# mask = tf.sequence_mask(self.sequence_lengths)
		# losses = tf.boolean_mask(losses, mask)
		self.loss = tf.reduce_mean(losses)

		# for tensorboard
		tf.summary.scalar('loss', self.loss)

	def train_rl(self, selector, train, dev, test):
		'''
		进行训练，采用了early stopping和学习率指数递减
		Args:
			train: dataset yields tuple of (sentence, tags)
			dev: dataset
		'''
		self.best_score = 0

		num_of_epoch_no_imprv = 0 # for early stopping，用来记录几个epoch没有提高了
		for epoch in range(self.hp.epochs):
			self.logger.info('Epoch {:} out of {:}'.format(epoch + 1, self.hp.epochs))
			
			# 运行一个epoch的训练工作，并返回在dev数据集上的测试f1
			score = self.jointly_train_selector_and_argument_detector(selector, train, dev, test, epoch)
			# 进行learning rate decay
			selector.hp.learning_rate *= self.hp.learning_rate_decay
			self.hp.learning_rate *= self.hp.learning_rate_decay

			# 进行early stopping并且保存最好的参数
			# 如果效果更好了
			if score > self.best_score:
				# 清零
				num_of_epoch_no_imprv = 0
				# 记录当前的参数
				self.save_session()
				# 更新best score
				self.best_score = score
				self.logger.info("- new best score! ")
			# 如果效果没有更好
			else:
				num_of_epoch_no_imprv += 1
				# 如果已经好多轮没有效果更好了，并且超过了25轮
				if num_of_epoch_no_imprv >= self.hp.num_of_epoch_no_imprv:
					self.logger.info("- early stopping {} epochs without "\
						"improvement".format(self.hp.num_of_epoch_no_imprv))
					break
		self.logger.info('Training model is over')


	def jointly_train_selector_and_argument_detector(self, selector, train, dev, test, epoch):
		'''
		进行联合训练
		'''

		'''
		1.0 训练entity selector
		'''
		self.logger.info('start to train the entity selector')

		loss = 0.0
		reward = 0.0 # 用来计算
		# 用于构造entity selector的训练数据
		event_type_batch_for_selector, event_sub_types_batch_for_selector, argument_role_batch_for_selector, \
		chars_batch_for_selector, entity_sub_types_batch_for_selector, entity_types_batch_for_selector, \
		roles_batch_for_selector, mask_for_selector, labels_for_selector, pos_tag_types_batch_for_selector = [],[],[],[],[],[],[],[],[],[]

		sentence_count = 0 # 记录处理过的句子数量
		entity_count = 0 # 记录处理过的entity数量

		# 构造selector梯度，全为零
		selector_gradient = []
		for variable in selector.variables:
			selector_gradient.append(np.zeros(variable.shape))

		# rewards
		rewards = []
		# gradients
		gradients = {} # {variable:[gradients]}
		
		# 一个句子一个句子进行处理
		for (event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
			entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, event_sub_types_batch, \
			pos_tag_types_batch, lexical_trigger_labels_batch, lexical_entity_labels_batch) in tqdm(train.get_sentence_batch()):
			# 句子数量+1
			sentence_count += 1
			
			# 进行多次sample
			for _ in range(self.hp.sample_epoch):

				# 需要循环地处理每个entity
				length = len(chars_batch[0]) # 句子长度
				entity_nums = len(chars_batch) # 实体数量
				# self.logger.info(str(chars_batch))
				# self.logger.info('entity数量为'+str(entity_nums))
				entity_count += entity_nums

				event_type_batch_selector = []
				idx_to_entity = {} # {entity end idx: idx} 表示entity结尾位置到entity的映射，这里需要注意多个entity可能处于同一个结尾位置

				# 1. 初始化
				# 记录每个entity的信息，给出mask
				mask_vector = [-1000000.0 for _ in range(length)]
				
				i = 0
				while i < entity_nums:
					# 第i个entity，用To-Predict来进行标示
					entity_start = roles_batch[i].index(71)
					entity_end = entity_start + roles_batch[i].count(71) - 1
					if entity_end not in idx_to_entity:
						idx_to_entity[entity_end] = []

					idx_to_entity[entity_end].append(i)
					# 利用end的表示结果来进行判断
					mask_vector[entity_end] = 0.0
					i += 1

				# 预测出的argument roles的结果，当前已知的论元
				predicted_roles = [self.argument_role_bio_vocab['O'] for _ in range(length)]

				# 2. 进行强化学习部分的预测，选出action，并且经过detector进行前向传播，并收集reward
				i = 0
				while i < entity_nums:
					# 2.1 喂给selector进行选择，直接选最大，不做采样
					# labels_pred, _ = selector.predict_batch(self.sess, [event_type_batch[i]], [event_sub_types_batch[i]],\
					# 	[argument_role_batch[i]], [chars_batch[i]], [entity_sub_types_batch[i]], [entity_types_batch[i]], \
					# 	[predicted_roles], [distances_1_batch[i]], [distances_2_batch[i]],[mask_vector])\
					
					# 2.1 喂给selector进行选择，并且进行采样
					# 这里的distance、argument_role_batch等很多信息实际上没用
					# self.logger.info(str(i))
					# self.logger.info(str(mask_vector))
					labels_pred, _ = selector.predict_batch_and_sample(self.sess, [event_type_batch[i]], [event_sub_types_batch[i]],\
						[argument_role_batch[i]], [chars_batch[i]], [entity_sub_types_batch[i]], [entity_types_batch[i]], \
						[predicted_roles],[mask_vector], [pos_tag_types_batch[i]])

					# 2.2 解析得到的结果，将其mask_vector设置为"-inf"
					entity_selected_end_idx = labels_pred[0]
					entity_selected_idx = idx_to_entity[entity_selected_end_idx].pop(0) # 预测得到的是第几个entity
					# 如果是这个位置的entity结束了，就设置为负无穷
					if idx_to_entity[entity_selected_end_idx] == []:
						mask_vector[entity_selected_end_idx] = -1000000.0

					# 2.3 构造entity selector的数据
					# event_type_batch_for_selector.append(event_type_batch[i])
					# event_sub_types_batch_for_selector.append(event_sub_types_batch[i])
					# argument_role_batch_for_selector.append(argument_role_batch[i])
					# chars_batch_for_selector.append(chars_batch[i])
					# entity_sub_types_batch_for_selector.append(entity_sub_types_batch[i])
					# entity_types_batch_for_selector.append(entity_types_batch[i])
					# roles_batch_for_selector.append(list(predicted_roles))
					# mask_for_selector.append(list(mask_vector))
					# selector选择的对象
					labels = [0 for _ in range(length)]
					labels[entity_selected_end_idx] = 1
					# labels_for_selector.append(labels)

					# 计算梯度
					selector_grads = selector.calculate_gradient(self.sess, [event_type_batch[i]], [event_sub_types_batch[i]],\
						[argument_role_batch[i]], [chars_batch[i]], [entity_sub_types_batch[i]], [entity_types_batch[i]], \
						[predicted_roles],[mask_vector], [labels], [pos_tag_types_batch[i]])

					# 2.4 将选中的entity喂给argument detector
					# 第j个entity需要进行预测
					j = entity_selected_idx
					entity_start = roles_batch[j].index(71)
					entity_end = entity_start + roles_batch[j].count(71) - 1

					# 添加'To-Predict'标签，确保覆盖现有标签
					for k in range(entity_start, entity_end + 1):
						predicted_roles[k] = self.argument_role_bio_vocab['To-Predict']

					labels_pred, loss, sequence_lengths = self.predict_batch([event_type_batch[j]], [event_sub_types_batch[j]],[argument_role_batch[j]], \
							[chars_batch[j]], [entity_sub_types_batch[j]], [entity_types_batch[j]], [predicted_roles], [distances_1_batch[j]], \
							[distances_2_batch[j]], [pos_tag_types_batch[j]] ,[lexical_trigger_labels_batch[j]], [lexical_entity_labels_batch[j]])

					# 计算reward
					# reward = - math.log(1 - math.e ** (-loss))
					reward = loss
					reward = - math.log(1 - math.e ** (-reward))

					# 统计梯度
					for gradient_idx, (grad, variable) in enumerate(zip(selector_grads, selector.variables)):
						# 如果是embedding类型的梯度，那么是indexedslices，需要进行统计
						if 'embedding' in str(variable):
							# 每个index进行遍历
							for idx_ in range(grad.values.shape[0]):
								selector_gradient[gradient_idx][grad.indices[idx_]] += grad.values[idx_] * reward
						# 如果不是embedding类型的梯度，那么是numpy.array类型，直接进行累加即可
						else:
							selector_gradient[gradient_idx] += grad * reward

			

					# 解析得到预测到的标签
					argument_role = self.idx_to_argument_role_tag[labels_pred[0]]

					# 2.6 更新环境

					# 如果采用ground truth来进行更新，就使用标记语料给出的标签
					if self.args.use_ground_truth:
						argument_role = self.idx_to_argument_role_tag[argument_role_batch[j]]
					# 否则就用前面预测到的标签
					else:
						pass

					# self.logger.info('预测到的类别是' + argument_role)
					# 如果预测结果不是O，那么就是argument role，需要进行标记
					if argument_role != 'O':
						# print('预测结果为'+argument_role)
						# print('预测结果为'+argument_role)
						# 1. 标记B
						argument_role_idx = self.argument_role_bio_vocab['B-' + argument_role]
						predicted_roles[entity_start] = argument_role_idx
						argument_role_idx = self.argument_role_bio_vocab['I-' + argument_role]
						# 2. 标记I
						for k in range(entity_start + 1, entity_end + 1):
							predicted_roles[k] = argument_role_idx
					# 如果预测结果是O
					else:
						argument_role_idx = self.argument_role_bio_vocab['O']
						for k in range(entity_start, entity_end + 1):
							predicted_roles[k] = argument_role_idx

					i += 1

			# 	break
			# break

			# 3. 更新entity selector

			# 每30个句子进行一次selector的训练
			if sentence_count % 30 == 0:
				
				# 计算reward，方式参考OpenNRE
				# reward = reward / entity_count
				# reward = - math.log(1 - math.e ** (-reward))
				# self.logger.info('reward is {}'.format(reward))

				# 进行平均
				for grad in selector_gradient:
					grad /= entity_count
				# selector进行更新
				selector.apply_gradient(self.sess, selector_gradient)

				# 清零
				reward = 0.0
				entity_count = 0
				selector_gradient = []
				for variable in selector.variables:
					selector_gradient.append(np.zeros(variable.shape))

			
		# 处理最后一组数据，可能不够30个
		if event_type_batch_for_selector != []:

			# 进行平均
			for grad in selector_gradient:
				grad /= entity_count
			# selector进行更新
			selector.apply_gradient(self.sess, selector_gradient)

			# 清零
			reward = 0.0
			entity_count = 0
			selector_gradient = []
			for variable in selector.variables:
				selector_gradient.append(np.zeros(variable.shape))

		# 打印loss
		# self.logger.info("train loss is " + str(loss / len(train)))


		# 在用reward矫正了之后，按照OpenNRE的设置，应该再来一轮训练，weight为1进行正常的梯度回传


		# 如果不对argument detector进行训练，那么agent训练完毕就进行返回
		if self.args.jointly_train_detector == 0:
			self.logger.info("start to run evaluation")
			
			# 测试test集合
			metrics = self.run_evaluate_rl(selector, test)
			msg = " - ".join(["{} {:04.2f}".format(k, v)
				for k, v in metrics.items()])
			self.logger.info(msg)
			# 进行dev集合
			metrics = self.run_evaluate_rl(selector, dev)
			msg = " - ".join(["{} {:04.2f}".format(k, v)
				for k, v in metrics.items()])
			self.logger.info(msg)


			return metrics["f1_2"]

		'''
		2.0 训练 argument detector
		'''
		self.logger.info('start to train the argument detector')
		loss = 0.0
		sentence_count = 0 # 记录处理过的句子数量
		# 用于构造entity selector的训练数据
		event_type_batch_for_detector, event_sub_types_batch_for_detector, argument_role_batch_for_detector, \
		chars_batch_for_detector, entity_sub_types_batch_for_detector,entity_types_batch_for_detector, \
		roles_batch_for_detector, distances_1_batch_for_detector, \
		distances_2_batch_for_detector, mask_for_detector, labels_for_detector, \
		pos_tag_types_batch_for_detector, lexical_trigger_labels_batch_for_detector, lexical_entity_labels_batch_for_detector \
		 = [],[],[],[],[],[],[],[],[],[],[],[],[],[]

		# 目前一个句子一个句子进行处理
		for (event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
			entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, event_sub_types_batch, \
			pos_tag_types_batch, lexical_trigger_labels_batch, lexical_entity_labels_batch) in tqdm(train.get_sentence_batch()):

			# rewards
			rewards = []
			# 需要循环地处理每个entity
			length = len(chars_batch[0]) # 句子长度
			entity_nums = len(chars_batch) # 实体数量

			event_type_batch_selector = []
			idx_to_entity = {} # {entity end idx: idx} 表示entity结尾位置到entity的映射，这里需要注意多个entity可能处于同一个结尾位置

			# 1. 初始化
			# 记录每个entity的信息，给出mask
			mask_vector = [-1000000.0 for _ in range(length)]
			i = 0
			while i < entity_nums:
				# 第i个entity
				entity_start = roles_batch[i].index(71)
				entity_end = entity_start + roles_batch[i].count(71) - 1
				if entity_end not in idx_to_entity:
					idx_to_entity[entity_end] = []
				# else:
				# 	self.logger.info('出现了重叠')
				idx_to_entity[entity_end].append(i)
				# 利用end的表示结果来进行判断
				mask_vector[entity_end] = 0.0
				i += 1
			# 预测出的argument roles的结果
			predicted_roles = [self.argument_role_bio_vocab['O'] for _ in range(length)]

			# 2. 进行强化学习部分的预测，选出action，并且经过detector进行前向传播，并收集reward
			i = 0
			while i < entity_nums:

				entity_count += 1

				# 2.1 喂给selector进行选择
				labels_pred, _ = selector.predict_batch(self.sess, [event_type_batch[i]], [event_sub_types_batch[i]],\
					[argument_role_batch[i]], [chars_batch[i]], [entity_sub_types_batch[i]], [entity_types_batch[i]], \
					[predicted_roles],[mask_vector], [pos_tag_types_batch[i]])
				# self.logger.info('预测entity结束位置为' + str(labels_pred[0]))

				# 2.2 解析得到的结果，将其mask_vector设置为"-inf"
				entity_selected_end_idx = labels_pred[0]
				entity_selected_idx = idx_to_entity[entity_selected_end_idx].pop(0)
				# 如果是这个位置的entity结束了，就设置为负无穷
				if idx_to_entity[entity_selected_end_idx] == []:
					mask_vector[entity_selected_end_idx] = -1000000.0


				# 2.4 将选中的entity喂给argument detector
				with tf.device('/cpu:0'):
					# 第j个entity需要进行预测
					j = entity_selected_idx
					entity_start = roles_batch[j].index(71)
					entity_end = entity_start + roles_batch[j].count(71) - 1

					# 添加'To-Predict'标签，确保覆盖现有标签
					for k in range(entity_start, entity_end + 1):
						predicted_roles[k] = self.argument_role_bio_vocab['To-Predict']

				# 2.5 构造detector的训练数据
				event_type_batch_for_detector.append(event_type_batch[j])
				event_sub_types_batch_for_detector.append(event_sub_types_batch[j])
				argument_role_batch_for_detector.append(argument_role_batch[j])
				chars_batch_for_detector.append(chars_batch[j])
				entity_sub_types_batch_for_detector.append(entity_sub_types_batch[j])
				entity_types_batch_for_detector.append(entity_types_batch[j])
				roles_batch_for_detector.append(list(predicted_roles))
				distances_1_batch_for_detector.append(distances_1_batch[j])
				distances_2_batch_for_detector.append(distances_2_batch[j])
				mask_for_selector.append(list(mask_vector))
				lexical_trigger_labels_batch_for_detector.append(lexical_trigger_labels_batch[j])
				lexical_entity_labels_batch_for_detector.append(lexical_entity_labels_batch[j])
				pos_tag_types_batch_for_detector.append(pos_tag_types_batch[j])

				# 2.6 进行前向传播，进行argument状态更新
				labels_pred, train_loss, sequence_lengths = self.predict_batch([event_type_batch[j]], [event_sub_types_batch[j]],[argument_role_batch[j]], \
						[chars_batch[j]], [entity_sub_types_batch[j]], [entity_types_batch[j]], [predicted_roles], [distances_1_batch[j]], \
						[distances_2_batch[j]], [pos_tag_types_batch[j]], [lexical_trigger_labels_batch[j]], [lexical_entity_labels_batch[j]])

				# 添加reward
				rewards.append(train_loss)

				argument_role = self.idx_to_argument_role_tag[labels_pred[0]]

				# 如果采用ground truth来进行更新，就使用标记语料给出的标签
				if self.args.use_ground_truth:
					argument_role = self.idx_to_argument_role_tag[argument_role_batch[j]]
				# 否则就用前面预测到的标签
				else:
					pass


				# self.logger.info('预测到的类别是' + argument_role)
				# 如果预测结果不是O，那么就是argument role，需要进行标记
				if argument_role != 'O':
					# print('预测结果为'+argument_role)
					# print('预测结果为'+argument_role)
					# 1. 标记B
					argument_role_idx = self.argument_role_bio_vocab['B-' + argument_role]
					predicted_roles[entity_start] = argument_role_idx
					argument_role_idx = self.argument_role_bio_vocab['I-' + argument_role]
					# 2. 标记I
					for k in range(entity_start + 1, entity_end + 1):
						predicted_roles[k] = argument_role_idx
				# 如果预测结果是O
				else:
					argument_role_idx = self.argument_role_bio_vocab['O']
					for k in range(entity_start, entity_end + 1):
						predicted_roles[k] = argument_role_idx
				i += 1
			loss += sum(rewards)

			# 进行更新
			if sentence_count % 50 == 0:

				# 进行训练
				self.run_train_op(
					event_type_batch_for_detector, \
					argument_role_batch_for_detector, \
					chars_batch_for_detector, \
					entity_sub_types_batch_for_detector, \
					entity_types_batch_for_detector,\
					roles_batch_for_detector, \
					distances_1_batch_for_detector, \
					distances_2_batch_for_detector, \
					event_sub_types_batch_for_detector, \
					pos_tag_types_batch_for_detector, \
					lexical_trigger_labels_batch_for_detector, \
					lexical_entity_labels_batch_for_detector \
					)

				# 然后清零
				event_type_batch_for_detector, event_sub_types_batch_for_detector, argument_role_batch_for_detector, \
				chars_batch_for_detector, entity_sub_types_batch_for_detector,entity_types_batch_for_detector, \
				roles_batch_for_detector, distances_1_batch_for_detector, \
				distances_2_batch_for_detector, mask_for_detector, labels_for_detector, \
				pos_tag_types_batch_for_detector, lexical_trigger_labels_batch_for_detector, lexical_entity_labels_batch_for_detector \
				 = [],[],[],[],[],[],[],[],[],[],[],[],[],[]
		
		# 处理剩下的部分
		if chars_batch_for_detector != []:
			self.run_train_op(
				event_type_batch_for_detector, \
				argument_role_batch_for_detector, \
				chars_batch_for_detector, \
				entity_sub_types_batch_for_detector, \
				entity_types_batch_for_detector,\
				roles_batch_for_detector, \
				distances_1_batch_for_detector, \
				distances_2_batch_for_detector, \
				event_sub_types_batch_for_detector, \
				pos_tag_types_batch_for_detector, \
				lexical_trigger_labels_batch_for_detector, \
				lexical_entity_labels_batch_for_detector \
				)


		# 打印loss
		self.logger.info("train loss is " + str(loss / len(train)))

		# 测试test集合
		metrics = self.run_evaluate_rl(selector, test)
		msg = " - ".join(["{} {:04.2f}".format(k, v)
			for k, v in metrics.items()])
		self.logger.info(msg)
		# 进行dev集合
		metrics = self.run_evaluate_rl(selector, dev)
		msg = " - ".join(["{} {:04.2f}".format(k, v)
			for k, v in metrics.items()])
		self.logger.info(msg)

		return metrics["f1_2"]

	def run_train_op(self, event_type_batch, argument_role_batch, chars_batch, \
		entity_sub_types_batch, entity_types_batch, roles_batch, \
		distances_1_batch, distances_2_batch, event_sub_types_batch, \
		pos_tag_types_batch, lexical_trigger_labels_batch, lexical_entity_labels_batch):
		'''
		根据batch的数据作为输入来对模型进行更新
		'''
		# 构造数据
		fd, _ = self.get_feed_dict(event_type_batch, event_sub_types_batch, \
			argument_role_batch, chars_batch, entity_sub_types_batch,\
			entity_types_batch, roles_batch, distances_1_batch, distances_2_batch,\
			lexical_trigger_labels_batch, lexical_entity_labels_batch,\
			pos_tag_types_batch, self.hp.learning_rate, self.hp.dropout)
		# 执行计算	
		_, train_loss = self.sess.run(
			[self.train_op, self.loss],
			feed_dict = fd
			)


	def run_epoch(self, train, dev, epoch):
		'''
		运行一个epoch，包括在训练集上训练、dev集上测试，一个epoch中对train集合的所有数据进行训练
		'''
		batch_size = self.hp.batch_size
		nbatches = (len(train) + batch_size - 1) // batch_size
		loss = 0.0

		# chars_batch, labels_batch, labels_17_batch, labels_3_batch, lm_embeddings_batch, segs_batch, words_batch
		for i, (event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch, entity_types_batch, roles_batch, \
			distances_1_batch, distances_2_batch, event_sub_types_batch, pos_tag_types_batch, lexical_trigger_labels_batch, lexical_entity_labels_batch) \
			in enumerate(train.get_minibatch(batch_size)):

			# 检查wo
			# 构造feed_dict,
			with tf.device('/cpu:0'):
				fd, _ = self.get_feed_dict(event_type_batch, event_sub_types_batch, \
					argument_role_batch, chars_batch, entity_sub_types_batch,\
					entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, \
					pos_tag_types_batch, lexical_trigger_labels_batch, \
					lexical_entity_labels_batch, self.hp.learning_rate, self.hp.dropout)

			# 执行计算	
			_, train_loss = self.sess.run(
				[self.train_op, self.loss],
				feed_dict = fd
				)
			loss += train_loss

			# # tensorboard
			# if i % 10 == 0:
			# 	self.file_writer.add_summary(summary, epoch * nbatches + i)
			# break
		self.logger.info("train loss is " + str(loss / len(train)))

		# 在dev集上面进行测试
		metrics = self.run_evaluate(dev)

		msg = " - ".join(["{} {:04.2f}".format(k, v)
			for k, v in metrics.items()])
		self.logger.info(msg)
		# self.logger.info("dev instance数量为" + str(self.dev_instance_num))

		# 返回f1
		return metrics["f1_2"]

	def pad_sequences(self, sequences, pad_token, nlevels = 1):
		'''
		对sequence进行填充
		Args:
			sequences: a generator of list or tuple
			pad_token: the token to pad with
			nlevels: padding的深度，如果是1，则表示对词进行填充，如果是2表示对字进行填充
		Return:
			a list of list where each sublist has same length
		'''
		# print '--------pad-sequences--------'
		if nlevels == 1:
			# 找到sequences中的句子最大长度
			# max_length_sentence = max(map(lambda x: len(x), sequences))
			# 然后直接进行padding
			sequences_padded, sequences_length = self._pad_sequences(sequences, 
				pad_token, self.hp.max_length_sentence)

		if nlevels == 2:
			# 找到sequence中所有句子的所有单词中字母数最大的单词
			max_length_word = max([max(map(lambda x: len(x), seq)) for seq in sequences])
			# print max_length_word
			sequences_padded, sequences_length = [], []
			for seq in sequences:
				# print seq
				# 将每个句子的每个词都进行填充
				sp, sl = self._pad_sequences(seq, pad_token, max_length_word)
				# print sp, sl
				# 每个句子的字母的表示
				sequences_padded += [sp]
				# 每个句子的字母的长度
				sequences_length += [sl]
			# 然后对句子进行填充
			# batch中最大长度的句子
			max_length_sentence = max(map(lambda x : len(x), sequences))
			# 填充的时候用[0,0,0,0,0]用字母向量进行填充
			sequences_padded, _ = self._pad_sequences(sequences_padded, 
				[pad_token] * max_length_word, max_length_sentence)
			# 得到句子的每个单词的字母的长度 (batch, max_length_sentence, letter_length)
			sequences_length, _ = self._pad_sequences(sequences_length, 0, max_length_sentence)

		return sequences_padded, sequences_length



	def _pad_sequences(self, sequences, pad_token, max_length):
		'''
		对sequences进行填充
		Args:
			pad_token: the token to pad with
		'''
		sequences_padded, sequences_lengths = [], []
		for sequence in sequences:
			sequence = list(sequence)
			# 获取句子长度
			sequences_lengths += [min(len(sequence), max_length)]
			# 进行填充
			sequence = sequence[:max_length] + [pad_token] * max(max_length - len(sequence), 0)
			sequences_padded += [sequence]
		return sequences_padded, sequences_lengths

	def pad_lm_embedding(self, lm_embeddings, pad_token):
		'''
		对语言模型进行padding
		'''
		# 找到batch中句子最大长度
		max_length_sentence = max(map(lambda x: len(x), lm_embeddings))	
		# print('最大句子长度为'+ str(max_length_sentence))
		# 然后直接进行padding
		sequences_padded, sequences_length = self._pad_sequences(lm_embeddings, 
			[pad_token] * lm_embedding_dim * len(bert_layers), max_length_sentence)
		return sequences_padded, sequences_length


	def get_feed_dict(self, event_type_batch, event_sub_types_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
		entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, pos_tag_types_batch, lexical_trigger_labels_batch, lexical_entity_labels_batch, lr = None, dropout = None):
		'''
		Args:
			lr: learning rate
			dropout: keep prob
		'''
		
		padding_idx = -1
		chars_batch_, sequence_lengths = self.pad_sequences(chars_batch, padding_idx)
		entity_sub_types_batch_, _ = self.pad_sequences(entity_sub_types_batch, padding_idx)
		event_sub_types_batch_, _ = self.pad_sequences(event_sub_types_batch, padding_idx)
		entity_types_batch_, _ = self.pad_sequences(entity_types_batch, padding_idx)
		roles_batch_, _ = self.pad_sequences(roles_batch, padding_idx)
		distances_1_batch_ = [[x - self.hp.distance_min for x in sentence_distance] for sentence_distance in distances_1_batch]
		distances_2_batch_ = [[x - self.hp.distance_min for x in sentence_distance] for sentence_distance in distances_2_batch]		
		distances_1_batch_, _ = self.pad_sequences(distances_1_batch_, padding_idx)
		distances_2_batch_, _ = self.pad_sequences(distances_2_batch_, padding_idx)
		pos_tag_types_batch_, _ = self.pad_sequences(pos_tag_types_batch, padding_idx)
		# padding UNK字符，全零向量
		lexical_entity_labels_batch_, _ = self.pad_sequences(lexical_entity_labels_batch, padding_idx)
		lexical_trigger_labels_batch_, _ = self.pad_sequences(lexical_trigger_labels_batch, padding_idx)


		feed = {
			self.char_ids: chars_batch_, # 句子的词的id表示(batch, max_length_sentence)
			self.sequence_lengths: sequence_lengths, # 句子的词的长度(batch, )
			self.event_type_labels: event_type_batch,
			self.argument_role_labels : argument_role_batch,
			self.entity_type_labels : entity_types_batch_,
			self.entity_sub_type_labels: entity_sub_types_batch_,
			self.event_sub_type_labels: event_sub_types_batch_,
			self.argument_role_labels_per_token:roles_batch_,
			self.distance_1_labels: distances_1_batch_,
			self.distance_2_labels: distances_2_batch_,
			self.pos_tag_ids: pos_tag_types_batch_,
			self.lexical_entity_labels: lexical_entity_labels_batch_,
			self.lexical_trigger_labels: lexical_trigger_labels_batch_
		}

		if lr is not None:
			feed[self.lr] = lr
		if dropout is not None:
			feed[self.dropout] = dropout
		
		return feed, sequence_lengths

	def run_evaluate_rl(self, selector, test, output_all_file = None, output_wrong_file = None, output_class_wrong_file = None):
		'''
		在测试集上运行，并且统计结果，包括precision/recall/accuracy/f1
		Args:
			test: 一个dataset instance
		Returns:
			metrics: dict metrics['acc'] = 98.4
		'''
		accs = []
		# 进行identification的记录
		correct_preds_id, total_correct_id, total_preds_id = 0., 0., 0. 
		# 进行classification的记录
		correct_preds_cl, total_correct_cl, total_preds_cl = 0., 0., 0.


		# 目前一个句子一个句子进行处理
		for (event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
			entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, event_sub_types_batch, \
			pos_tag_types_batch, lexical_trigger_labels_batch, lexical_entity_labels_batch) in test.get_sentence_batch():
			# 记录顺序
			order = []
			
			# 需要循环地处理每个entity
			length = len(chars_batch[0]) # 句子长度
			entity_nums = len(chars_batch) # 实体数量

			event_type_batch_selector = []
			idx_to_entity = {} # {entity end idx: idx}

			# 1. 初始化
			# 记录每个entity的信息，给出mask
			mask_vector = [-1000000.0 for _ in range(length)]
			i = 0
			while i < entity_nums:
				# 第i个entity
				entity_start = roles_batch[i].index(71)
				entity_end = entity_start + roles_batch[i].count(71) - 1
				if entity_end not in idx_to_entity:
					idx_to_entity[entity_end] = []
				# else:
				# 	self.logger.info('出现了重叠')
				idx_to_entity[entity_end].append(i)
				# 利用end的表示结果来进行判断
				mask_vector[entity_end] = 0.0
				i += 1
			# 预测出的argument roles的结果
			predicted_roles = [self.argument_role_bio_vocab['O'] for _ in range(length)]

			# 2. 进行强化学习部分的预测
			i = 0
			while i < entity_nums:

				# 2.1 喂给selector进行选择
				labels_pred, _ = selector.predict_batch(self.sess, [event_type_batch[i]], [event_sub_types_batch[i]],\
					[argument_role_batch[i]], [chars_batch[i]], [entity_sub_types_batch[i]], [entity_types_batch[i]], \
					[predicted_roles], [mask_vector], [pos_tag_types_batch[i]])

				# 2.2 解析得到的结果，将其mask_vector设置为"-inf"
				entity_selected_end_idx = labels_pred[0]
				entity_selected_idx = idx_to_entity[entity_selected_end_idx].pop(0)
				# 如果是这个位置的entity结束了，就设置为负无穷
				if idx_to_entity[entity_selected_end_idx] == []:
					mask_vector[entity_selected_end_idx] = -1000000.0

				# 2.4 将选中的entity喂给argument detector
				with tf.device('/cpu:0'):
					# 第j个entity需要进行预测
					j = entity_selected_idx
					order.append(j)
					entity_start = roles_batch[j].index(71)
					entity_end = entity_start + roles_batch[j].count(71) - 1

					# 添加'To-Predict'标签，确保覆盖现有标签
					for k in range(entity_start, entity_end + 1):
						predicted_roles[k] = self.argument_role_bio_vocab['To-Predict']


					# fd, _ = self.get_feed_dict([event_type_batch[j]], [event_sub_types_batch[j]],[argument_role_batch[j]], \
					# 	[chars_batch[j]], [entity_sub_types_batch[j]], [entity_types_batch[j]], [predicted_roles], [distances_1_batch[j]], \
					# 	[distances_2_batch[j]],[lexical_trigger_labels_batch[j]], [lexical_entity_labels_batch[j]],self.hp.learning_rate, self.hp.dropout)
				

				# 2.5 对该entity进行预测
				labels_pred, loss, sequence_lengths = self.predict_batch([event_type_batch[j]], [event_sub_types_batch[j]],[argument_role_batch[j]], \
						[chars_batch[j]], [entity_sub_types_batch[j]], [entity_types_batch[j]], [predicted_roles], [distances_1_batch[j]], \
						[distances_2_batch[j]], [pos_tag_types_batch[j]], [lexical_trigger_labels_batch[j]], [lexical_entity_labels_batch[j]])

				# 2.6 将预测的结果更新到predicted_roles上面
				argument_role = self.idx_to_argument_role_tag[labels_pred[0]]
				# 如果预测结果不是O，那么就是argument role，需要进行标记
				if argument_role != 'O':
					# print('预测结果为'+argument_role)
					# print('预测结果为'+argument_role)
					# 1. 标记B
					argument_role_idx = self.argument_role_bio_vocab['B-' + argument_role]
					predicted_roles[entity_start] = argument_role_idx
					argument_role_idx = self.argument_role_bio_vocab['I-' + argument_role]
					# 2. 标记I
					for k in range(entity_start + 1, entity_end + 1):
						predicted_roles[k] = argument_role_idx
				# 如果预测结果是O
				else:
					argument_role_idx = self.argument_role_bio_vocab['O']
					for k in range(entity_start, entity_end + 1):
						predicted_roles[k] = argument_role_idx

				i += 1

				# 解析标签并统计实验结果

				# 真实标签
				lab = argument_role_batch[j]
				lab_pred = labels_pred[0]

				# 如果测试样本是argument
				if lab != self.argument_role_vocab['O']:
					total_correct_id += 1
					total_correct_cl += 1

					# 如果识别正确
					if lab_pred != self.argument_role_vocab['O']:
						correct_preds_id += 1
					# 如果分类正确				
					if lab_pred == lab:
						correct_preds_cl += 1

				# 统计所有的识别和分类数量
				if lab_pred != self.argument_role_vocab['O']:
					total_preds_id += 1
					total_preds_cl += 1
			# 输出顺序，用于调试
			# self.logger.info(','.join([str(x) for x in order]))

		# 1. 计算trigger identification的结果
		# 计算precision，预测出的chunk中有多少是正确的
		p1 = correct_preds_id / total_preds_id if correct_preds_id > 0 else 0
		# 计算recall，预测正确的chunk占了所有chunk的数量
		r1 = correct_preds_id / total_correct_id if correct_preds_id > 0 else 0
		# 计算f1
		f1_1 = 2 * p1 * r1 / (p1 + r1) if correct_preds_id > 0 else 0

		# 2. 计算trigger classification的结果
		# 计算precision，预测出的chunk中有多少是正确的
		p2 = correct_preds_cl / total_preds_cl if correct_preds_cl > 0 else 0
		# 计算recall，预测正确的chunk占了所有chunk的数量
		r2 = correct_preds_cl / total_correct_cl if correct_preds_cl > 0 else 0
		# 计算f1
		f1_2 = 2 * p2 * r2 / (p2 + r2) if correct_preds_cl > 0 else 0
		# 计算accuracy，用预测对的词的比例来进行表示
		acc = np.mean(accs)

		# 返回结果
		return {
			'p1': 100 * p1,
			'r1': 100 * r1,
			'f1_1': 100 * f1_1,
			'p2': 100 * p2,
			'r2': 100 * r2,
			'f1_2': 100 * f1_2
		}

	def run_evaluate(self, test, output_all_file = None, output_wrong_file = None, output_class_wrong_file = None):
		'''
		'''
		# 如果是预训练状态，在测试的时候，就从左往右进行测试
		if self.stage == 0:
			self.logger.info('按照从左往右的顺序进行测试')
			return self.run_evaluate_loop(test, output_all_file, output_wrong_file, output_class_wrong_file)
		# 如果是联合训练状态，selector已经完成了或者正在训练，测试的时候就采用
		elif self.stage == 1:
			self.logger.info('按照agent确定的顺序进行测试')
			return self.run_evaluate_rl(self.selector, test, output_all_file, output_wrong_file, output_class_wrong_file)

	def run_evaluate_loop(self, test, output_all_file = None, output_wrong_file = None, output_class_wrong_file = None):
		'''
		在测试集上运行，并且统计结果，包括precision/recall/accuracy/f1
		Args:
			test: 一个dataset instance
		Returns:
			metrics: dict metrics['acc'] = 98.4
		'''
		accs = []
		# 进行identification的记录
		correct_preds_id, total_correct_id, total_preds_id = 0., 0., 0. 
		# 进行classification的记录
		correct_preds_cl, total_correct_cl, total_preds_cl = 0., 0., 0.
		
		
		# idx_to_token = {idx : tag for tag, idx in self.char_vocab.items()}
		# idx_to_tag = {idx: token for token, idx in self.argument_role_bio_vocab.items()}

		for (event_type_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
				entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, \
				pos_tag_types_batch, event_sub_types_batch, lexical_trigger_labels_batch, \
				lexical_entity_labels_batch) in test.get_sentence_batch():
			# predict_batch
			# shape = (batch size, max_length_sentence)
			# shape = (batch size,)
			idx = -1 # 表示这个batch中句子的index

			# 执行inference
			labels_pred = self.inference_with_self_loop([event_type_batch, event_sub_types_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
			entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, pos_tag_types_batch, lexical_trigger_labels_batch, lexical_entity_labels_batch])

			for lab, lab_pred in zip(argument_role_batch, labels_pred):
				# 如果测试样本是argument
				if lab != self.argument_role_vocab['O']:
					total_correct_id += 1
					total_correct_cl += 1

					# 如果识别正确
					if lab_pred != self.argument_role_vocab['O']:
						correct_preds_id += 1
					# 如果分类正确				
					if lab_pred == lab:
						correct_preds_cl += 1

				# 统计所有的识别和分类数量
				if lab_pred != self.argument_role_vocab['O']:
					total_preds_id += 1
					total_preds_cl += 1

			# break



		# 1. 计算trigger identification的结果
		# 计算precision，预测出的chunk中有多少是正确的
		p1 = correct_preds_id / total_preds_id if correct_preds_id > 0 else 0
		# 计算recall，预测正确的chunk占了所有chunk的数量
		r1 = correct_preds_id / total_correct_id if correct_preds_id > 0 else 0
		# 计算f1
		f1_1 = 2 * p1 * r1 / (p1 + r1) if correct_preds_id > 0 else 0

		# 2. 计算trigger classification的结果
		# 计算precision，预测出的chunk中有多少是正确的
		p2 = correct_preds_cl / total_preds_cl if correct_preds_cl > 0 else 0
		# 计算recall，预测正确的chunk占了所有chunk的数量
		r2 = correct_preds_cl / total_correct_cl if correct_preds_cl > 0 else 0
		# 计算f1
		f1_2 = 2 * p2 * r2 / (p2 + r2) if correct_preds_cl > 0 else 0
		# 计算accuracy，用预测对的词的比例来进行表示
		acc = np.mean(accs)

		# 返回结果
		return {
			'p1': 100 * p1,
			'r1': 100 * r1,
			'f1_1': 100 * f1_1,
			'p2': 100 * p2,
			'r2': 100 * r2,
			'f1_2': 100 * f1_2
		}

	def inference_with_self_loop(self, src_input):
		"""
		从左往右按顺序进行遍历，用模型t时刻预测出的标签来更新模型t+1时刻的输入，得到的是这个句子里的所有entity的argument role的预测
		"""
		predicted_results = []
		event_type_batch, event_sub_types_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
			entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, pos_tag_types_batch,\
			 lexical_trigger_labels_batch, lexical_entity_labels_batch = src_input	
		# 0. 取得长度
		length = len(chars_batch[0])
		entity_nums = len(chars_batch)
		# 1. 初始化argument roles，全为O
		# argument_roles = [self.argument_role_bio_vocab['O'] for _ in range(length)] # 表示输出的argument roles
		predicted_roles = [self.argument_role_bio_vocab['O'] for _ in range(length)] # 表示预测出的argument roles
		# print(argument_roles)
		# 2. 取出所有的entities span
		# entities = get_entities(entity_sub_types_batch[0],self.entity_sub_type_vocab)
		# print(entities)
		sequence_lengths = 0
		# 3. 开始进行循环
		i = 0
		while i < entity_nums:

			entity_start = roles_batch[i].index(71)
			entity_end = entity_start + roles_batch[i].count(71) - 1

			# 3.0 添加'To-Predict'标签，确保覆盖现有标签
			for j in range(entity_start, entity_end + 1):
				predicted_roles[j] = self.argument_role_bio_vocab['To-Predict']

			# 3.1 前向传播进行预测
			labels_pred, loss, sequence_lengths = self.predict_batch([event_type_batch[i]], [event_sub_types_batch[i]],[argument_role_batch[i]], \
				[chars_batch[i]], [entity_sub_types_batch[i]], [entity_types_batch[i]], [predicted_roles], [distances_1_batch[i]], \
				[distances_2_batch[i]], [pos_tag_types_batch[i]],[lexical_trigger_labels_batch[i]], [lexical_entity_labels_batch[i]])
			# 添加预测的标签
			predicted_results.append(labels_pred[0])
			# 3.2 对argument_roles的表示进行更新
			# argument_roles = roles_batch[0][:entity[1] + 1] + argument_roles[entity[1] + 1:]

			# 3.2 对argument_roles的表示进行更新
			# 1. 取出argument role tag
			argument_role = self.idx_to_argument_role_tag[labels_pred[0]]
			
			# 如果预测结果不是O，那么就是argument role，需要进行标记
			if argument_role != 'O':
				# print('预测结果为'+argument_role)
				# print('预测结果为'+argument_role)
				# 1. 标记B
				argument_role_idx = self.argument_role_bio_vocab['B-' + argument_role]
				predicted_roles[entity_start] = argument_role_idx
				argument_role_idx = self.argument_role_bio_vocab['I-' + argument_role]
				# 2. 标记I
				for j in range(entity_start + 1, entity_end + 1):
					predicted_roles[j] = argument_role_idx
			# 如果预测结果是O
			else:
				for j in range(entity_start, entity_end + 1):
					argument_role_idx = self.argument_role_bio_vocab['O']
					predicted_roles[j] = argument_role_idx

			i += 1


		# 4. 返回结果
		return predicted_results


	def get_chunks(self, seq, tags):
		'''
		给定一个序列的tags，将其中的entity和位置取出来
		Args:
			seq: [4,4,0,0,1,2...] 一个句子的label
			tags: dict['I-LOC'] = 2
		Returns:
			list of (chunk_type, chunk_start, chunk_end)

		Examples:
			seq = [4, 5, 0, 3]
			tags = {
			'B-PER' : 4,
			'I-PER' : 5,
			'B-LOC' : 3
			'O' : 0
			}

			Returns:
				chunks = [
					('PER', 0, 2),
					('LOC', 3, 4)
				]
		'''
		idx_to_tag = {idx : tag for tag, idx in tags.items()}
		# print idx_to_tag
		chunks = []

		# 表示当前的chunk的起点和类型
		chunk_start, chunk_type = None, None
		# print seq

		for i, tag_idx in enumerate(seq):
			# 如果不是entity的一部分
			if tag_idx == tags['O']:
				# 如果chunk_type不是None，那么就是一个entity的结束
				if chunk_type != None:
					chunk = (chunk_type, chunk_start, i)
					chunks.append(chunk)
					chunk_start, chunk_type = None, None
				# 如果chunk_type是None，那么就不需要处理
				else:
					pass
			# 如果是BI
			else:
				tag = idx_to_tag[tag_idx]
				# 如果是B
				if tag[0] == 'B':
					# 如果前面有entity，那么这个entity就完成了
					if chunk_type != None:
						chunk = (chunk_type, chunk_start, i)
						chunks.append(chunk)
						chunk_start, chunk_type = None, None

					# 记录开始
					chunk_start = i
					chunk_type = tag[2:]

				# 如果是I
				else:
					if chunk_type != None:
						# 如果chunk_type发生了变化，例如(B-PER, I-PER, I-LOC)，那么就需要将(B-PER, I-PER)归类为chunk
						if chunk_type != tag[2:]:
							chunk = (chunk_type, chunk_start, i)
							chunks.append(chunk)
							chunk_start, chunk_type = None, None
		
		# 处理可能存在的最后一个未结尾的chunk
		if chunk_type != None:
			chunk = (chunk_type, chunk_start, i + 1)
			chunks.append(chunk)
		return chunks


	def predict_batch(self, event_type_batch, event_sub_types_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
		entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, pos_tag_types_batch, lexical_trigger_labels_batch, lexical_entity_labels_batch):
		'''
		对一个batch进行预测，并返回预测结果
		Args:
			words: list of sentences
		Returns:
			labels_pred: list of labels for each sentence
		'''
		logits_sequences = []
		# chars_batch, segs_batch, words_batch, lm_embeddings_batch, labels_batch
		fd, sequence_lengths = self.get_feed_dict(event_type_batch, event_sub_types_batch, argument_role_batch, chars_batch, entity_sub_types_batch,\
		entity_types_batch, roles_batch, distances_1_batch, distances_2_batch, pos_tag_types_batch, lexical_trigger_labels_batch, lexical_entity_labels_batch, dropout = 1.0)
		# shape = (batch size,)
		labels_pred, loss = self.sess.run([self.labels_pred, self.loss], feed_dict = fd)

		return labels_pred, loss, sequence_lengths

if __name__ == '__main__':
	model = seq_tag_model(None, None, [None,None,None], None)
	seq = [4, 4, 5, 0, 3, 5]
	tags = {
			'B-PER' : 4,
			'I-PER' : 5,
			'B-LOC' : 3,
			'O' : 0
			}
	print(model.get_chunks(seq, tags))

