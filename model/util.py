#coding=utf-8
# from dataset import *
from .vocab import *
def get_entities(entity_types, entity_sub_type_vocab):
	"""
	根据entity的标注结果来获取entity的列表，entity_sub_types_batch
	"""
	entity_list = []
	start_idx, idx = None, 0
	while idx < len(entity_types):
		# 如果是B
		if entity_types[idx] <= 49:
			# 如果是 BB 或者BIB,就结束当前的entity，开始新的entity
			if start_idx is not None:
				entity_list.append([start_idx, idx - 1])
				start_idx = idx
			# 如果是新的开始
			else:
				start_idx = idx
		# 如果是O
		elif entity_types[idx] == 100:
			if start_idx is not None:
				entity_list.append([start_idx, idx - 1])
				start_idx = None
		# 如果是I
		else:
			# 如果跟之前相同无论是B/I
			if entity_types[idx-1] == entity_types[idx] or entity_types[idx-1] == entity_types[idx] - 50:
				idx += 1
				continue
			else:
				if start_idx is not None:
					entity_list.append([start_idx, idx - 1])
				start_idx = idx

		idx += 1
	# 处理剩下的未处理的部分
	if start_idx is not None:
		entity_list.append([start_idx, idx - 1])
	return entity_list

def get_distance(length, anchor, offset = 0):
	"""
	根据anchor的位置来返回distance向量
	length:
		序列长度
	anchor: [start_idx, end_idx], 可能表示trigger和entity的位置
	offset: 为了完成正数的表示，需要偏移多少
	"""
	start_idx, end_idx = anchor[0], anchor[1]
	distance = [idx - start_idx if idx < start_idx else idx - end_idx for idx in range(length)]
	distance = distance[:start_idx] + [0 for _ in range(start_idx, end_idx)] + distance[end_idx:]
	distance = [x + offset for x in distance]
	return distance

if __name__ == '__main__':
	entity_sub_type_vocab = load_vocab('../../Preprocess/data/processed/english/arguments/ltr/entity_sub_type_vocab.txt')
	a = [0,50,50,0,50,49,50,100,100,51]
	print(get_entities(a, entity_sub_type_vocab))
	print(get_distance(10, [5,6], 5))