#coding=utf-8
import sys
import os
sys.path.append("..")
from .dataset import *
import numpy as np
import io

from configure import *
import json

def strQ2B(ustring):
	"""全角转半角"""
	rstring = ""
	for uchar in ustring:
		inside_code=ord(uchar)
		if inside_code == 12288:
		#全角空格直接转换
			inside_code = 32 
		elif (inside_code >= 65281 and inside_code <= 65374):
			#全角字符（除空格）根据关系转化
			inside_code -= 65248

		rstring += chr(inside_code)
	return rstring

def add_word_info(data_id_lm_path, word_vocab):
	'''
	将词的信息加入到id文件中，包括两列，word id、seg id
	'''
	pass
def write_id_files_english(token, tag, token_vocab, tag_vocab, lm_embedding, f1, f2):
	'''
	写入文件
	Args:
		token: bert解析的token
		tag: tag
		token_vocab:{token} = token_id
		tag_vocab:{tag} = tag_id
		lm_embedding: 表示这个token的lm embedding，如果有就需要输出，如果没有就不输出
		f1:lm+id形式的输出
	'''
	# 1. 查找tag id
	tag_id = tag_vocab[tag]

	# 2. 解析token
	if token not in token_vocab:
		token_id = token_vocab[UNK_TOKEN]
	else:
		token_id = token_vocab[token]	
	# 3. 进行输出


	# 3.1 输出lm+id
	output_line = str(token_id) + ' ' + str(tag_id) + ' ' + '|'.join([str(x) for x in lm_embedding]) + ' ' + '0' + ' ' + '0' + '\n'
	f1.write(output_line)
	# 3.2 输出文本形式
	output_line = token + ' ' + tag + '\n'
	f2.write(output_line)

def write_id_files(token, tags, token_vocab, tag_vocab, lm_embedding, f1, f2):
	'''
	写入文件
	Args:
		token: bert解析的token
		tag: tag
		token_vocab:{token} = token_id
		tag_vocab:{tag} = tag_id
		lm_embedding: 表示这个token的lm embedding，如果有就需要输出，如果没有就不输出
		f1:lm+id形式的输出
		f2:文本形式的输出
	'''
	# 1. 解析tag
	_tags = set(tags)
	label = set()

	# 如果不统一，可能是B {I}xN的情况，这也需要处理
	if len(_tags) != 1:
		# 处理 B {I}xN的情况
		#500
		#['B-Transfer-Money', 'I-Transfer-Money', 'I-Transfer-Money']
		label = set()
		for tag in _tags:
			label.add(tag[2:])

		# 如果解析过的label有问题，那么就报错
		if len(label) != 1:
			print('---------------------')
			print(token)
			print(tags)
			raise Exception('出现了tags无法统一的情况')

	# 如果是B {I}xN的情况，那么就按照label的情况来取
	if len(_tags) != 1:
		tag = 'B-' + list(label)[0]
	# 否则就取原本的标签
	else:
		tag = list(_tags)[0]

	tag_id = tag_vocab[tag]

	# 2. 解析token
	if token not in token_vocab:
		token_id = token_vocab[UNK_TOKEN]
	else:
		token_id = token_vocab[token]
	
	# 3. 进行输出


	# 3.1 输出lm+id
	output_line = str(token_id) + ' ' + str(tag_id) + ' ' + '|'.join([str(x) for x in lm_embedding]) + '\n'
	f1.write(output_line)
	# 3.2 输出文本形式
	output_line = token + ' ' + tag + '\n'
	f2.write(output_line)

def getMaxLength(dataset):
	'''
	'''
	print('查找最大长度')
	large_sentences_num = 0
	max_length = 0
	max_line = None
	for event_type, argument_role, chars, entity_sub_types, entity_types, roles, distances_1, distances_2 in dataset:
		line = ''.join(chars)
		# print(line)
		if len(line) > max_length:
			max_length = len(line)
			max_line = line
		if len(line) > 510:
			large_sentences_num += 1
			print(line)
		
	return large_sentences_num, max_length, max_line

def convert_text_to_id_without_lm_embedding(dataset, output_file_id_path, \
	char_vocab, event_type_vocab, argument_role_vocab, entity_type_vocab, entity_sub_type_vocab):
	'''
	将文本文件转化为id文件
	'''
	with open(output_file_id_path, 'w') as f:
		# 每行
		for event_type, argument_role, chars, entity_sub_types, entity_types, roles, distances_1, distances_2 in dataset:
			# 先处理类别
			event_type_id = event_type_vocab[event_type]
			argument_role_id = argument_role_vocab[argument_role]
			# 进行输出
			output_line = str(event_type_id) + ' ' + str(argument_role_id) + '\n'

			f.write(output_line)
			# 然后处理每个字
			for char, e_s_t, e_t, r, dis_1, dis_2 in zip(chars, entity_sub_types, entity_types, roles, distances_1, distances_2):

				# if token not in token_vocab:
				# 	token_id = token_vocab[UNK_TOKEN]
				# else:
				# 	token_id = token_vocab[token]

				char_id = char_vocab[char]
				e_s_t_id = entity_sub_type_vocab[e_s_t]
				e_t_id = entity_type_vocab[e_t]
				r_id = argument_role_vocab[r]

				# 进行输出
				output_line = str(char_id) + ' ' + str(e_s_t_id) + ' ' + str(e_t_id) \
				+ ' ' + str(r_id) + ' ' + str(dis_1) + ' ' + str(dis_2) +  '\n'

				f.write(output_line)
			# 换行
			f.write('\n')	



# 处理bert的结果
def process_bert_result(line, layers = [-1]):
	'''
	处理bert输出文件中的一行
	'''
	bert_tokens = []
	bert_embeddings = []
	line = json.loads(line.strip())
	# 取出有效features
	features = line['features'][1:-1]
	
	# 遍历每个token
	for feature in features:
		bert_tokens.append(feature['token'])
		bert_embedding = []
		token_layers = feature['layers']
		for token_layer in token_layers:
			layer_id = token_layer['index']
			if layer_id in layers:
				bert_embedding.extend(token_layer['values'])
		bert_embeddings.append(bert_embedding)

	return bert_tokens, bert_embeddings

def convert_text_to_id_with_lm_embedding_english(dataset, output_file_path, output_file_id_path, token_vocab, tag_vocab, bert_output, layers = [-1]):
	'''
	将文本文件转化为id文件，同时得到token embedding、无效的词信息等
	Args:
		dataset: 表示文本文件处理的dataset
		output_file_path: 表示str文件的输出
		output_file_id_path: 表示id文件的输出
		token_vocab: 表示token字典
		tag_vocab: 表示tag字典
	'''
	f3 = open(bert_output,'r')

	with open(output_file_id_path, 'w') as f1:
		with open(output_file_path, 'w') as f2:
			for tokens, tags, _, _, _ in dataset:
				idx = 0
				line = ' '.join(tokens)

				# 统计出来有标记的词
				labeled_tokens = []
				labeled_labels = []
				for token, tag in zip(tokens, tags):
					if tag != 'O':
						
						labeled_labels.append(tag)
						if token[-1] == ',' or token[-1] == '.':
							labeled_tokens.append(token[:-1])
						else:
							labeled_tokens.append(token)


				tags_number = len(labeled_labels)

				# print(tokens)
				# print(tags)
				bert_line = f3.readline()
				bert_tokens, lm_embeddings = process_bert_result(bert_line,layers)
				for token, lm_embedding in zip(bert_tokens, lm_embeddings):


					token = token.replace('#', '')
					if token not in labeled_tokens:
						write_id_files(token, 'O', token_vocab, tag_vocab, lm_embedding, f1, f2)
					else:
						tag = labeled_labels[labeled_tokens.index(token)]
						write_id_files_english(token, tag , token_vocab, tag_vocab, lm_embedding, f1, f2)
						tags_number -= 1

					
				# break
				if tags_number != 0:
					print('-----------------------------------')
					print(line)
					print(tokens)
					print(tags)
					print(bert_tokens)
					print('有的tag没找到')
					# raise Exception('有的tag没找到')
				# 加入句子间隔
				f1.write('\n')
				f2.write('\n')
	f3.close()




	# bert_line = f3.readline()
	# bert_tokens, lm_embeddings = process_bert_result(bert_line,layers)
	# print(bert_tokens)



def convert_text_to_id_with_lm_embedding(dataset, output_file_path, output_file_id_path, token_vocab, tag_vocab, bert_output, layers = [-1]):
	'''
	将文本文件转化为id文件
	Args:
		dataset: 表示文本文件处理的dataset
		output_file_path: 表示str文件的输出
		output_file_id_path: 表示id文件的输出
		token_vocab: 表示token字典
		tag_vocab: 表示tag字典
	'''
	# extractor = bert_feature_extractor(bert_vocab_file, bert_config_file, bert_init_checkpoint, layers = [-1], 
	# 	max_seq_length = 512, batch_size = 8, do_lower_case = False, master = None, num_tpu_cores = 8, 
	# 	use_tpu = False, use_one_hot_embeddings = False)
	f3 = open(bert_output,'r')
	with open(output_file_id_path, 'w') as f1:
		with open(output_file_path, 'w') as f2:
			# 遍历每个句子
			for tokens, tags, _, _, _ in dataset:
				line = ''.join(tokens)
				# print(line)
				# 获取这行的embedding和bert的tokenization
				bert_line = f3.readline()
				bert_tokens, lm_embeddings = process_bert_result(bert_line,layers)
				# bert_tokens, lm_embeddings = extractor.getLMEmbeddingForSentence(line)
				# print(bert_tokens)

				# 根据bert_token来进行处理，bert_token可能会对一些字符进行合并

				idx = 0
				unknowns = [] # 表示没有处理的unknown对应的起点(idx, lm_embedding)
				# 遍历每个字
				for token, lm_embedding in zip(bert_tokens, lm_embeddings):
					
					# 防止出现##的情况
					# token = strQ2B(token.replace('#', ''))
					token = token.replace('#', '')

					# 如果是unknown，就需要下一次来进行处理
					if token == '[UNK]':
						# 如果已经有了unknown
						unknowns.append((idx,lm_embedding))
						continue

					# 如果不是unknown，那么需要处理可能的unknown
					if unknowns != []:
						print('--------------------------处理unknown---------------------')
						# 找到unknown的边界,以当前词的开始之前那个作为边界
						print('有效句子:' + line[idx:])
						print('当前token: ' + token)
						begin = line[idx:].index(token)

						# 找到unknown token
						unknown_token_end = begin + idx
						unknown_tokens = line[idx : unknown_token_end]
						print('unknown_tokens:' + unknown_tokens)
						# if len(unknowns) != 1:
						# 	print(unknown_tokens)
						# 	if len(unknown_tokens) != len(unknowns):
						# 		raise Exception('出现错误')
						if unknown_tokens[0] == '“':
							write_id_files(unknown_tokens[0], tags[idx:idx+1], token_vocab, tag_vocab, unknowns[0][1], f1, f2)
							unknown_tokens = unknown_tokens[1:]
							unknowns = unknowns[1:]

						unknowns_to_write = None
						if unknown_tokens != '' and unknown_tokens[-1] == '”':
							unknowns_to_write = (unknown_tokens[-1],tags[unknown_token_end-1:unknown_token_end],unknowns[-1][1])
							# write_id_files(unknown_tokens[-1], tags[unknown_token_end-1:unknown_token_end], token_vocab, tag_vocab, unknowns[-1][1], f1, f2)
							unknown_tokens = unknown_tokens[:-1]
							unknowns = unknowns[:-1]



						# 如果只有一个unknown
						if len(unknowns) == 1:
							write_id_files(unknown_tokens, tags[idx: unknown_token_end], token_vocab, tag_vocab, unknowns[0][1], f1, f2)

						else:
							# 如果有多个unknown
							# if unknown_tokens[0] == '“':
							# 	write_id_files(unknown_token[0], tags[idx], token_vocab, tag_vocab, unknowns[0][1], f1, f2)
							# 	unknown_tokens = unknown_tokens[1:]
							# 	unknowns = unknowns[1:]

							# if unknown_tokens[-1] == '”':
							# 	write_id_files(unknown_token[-1], tags[unknown_token_end], token_vocab, tag_vocab, unknowns[-1][1], f1, f2)
							# 	unknown_tokens = unknown_tokens[:-1]
							# 	unknowns = unknowns[:-1]

							if len(unknowns) != 1:
								print(unknown_tokens)
								if len(unknown_tokens) != len(unknowns):
									raise Exception('出现错误')


							unknown_tokens = [x for x in unknown_tokens]
							# 对unknown进行输出
							count = 0
							for unknown_token, unknown in zip(unknown_tokens, unknowns):
								count += 1
								write_id_files(unknown_token, tags[idx: idx + count], token_vocab, tag_vocab, unknown[1], f1, f2)

						unknowns = []

						# 写最后的”
						if unknowns_to_write != None:
							write_id_files(unknowns_to_write[0], unknowns_to_write[1], token_vocab, tag_vocab, unknowns_to_write[2], f1, f2)


						idx = unknown_token_end # 对idx进行处理，否则沿用上次的idx



					# 然后继续处理本次的token
					length = len(token)

					# 进行检查,如果与原文不一致
					if token != ''.join(tokens[idx : length + idx]):
						print(bert_tokens)
						print(line)
						print(token)
						print(''.join(tokens[idx : length + idx]))
						raise Exception('出现了token无法统一的情况')
					# 检查通过进行输出

					
					write_id_files(token, tags[idx : idx + length], token_vocab, tag_vocab, lm_embedding, f1, f2)
					idx += length
				# 处理最后可能的unknown, 只能赋给idx这一位
				if unknowns != []:


					print('--------------------------处理结尾unknown---------------------')
					for unknown in unknowns:
						print(line[unknown[0]])
					print(line)

					# # 找到unknown的边界,以当前词的开始之前那个作为边界
					# print(line[idx:])
					# print(token)
					# begin = line[idx:].index(token)

					# 找到unknown token
					count = 0
					for unknown in unknowns:
						unknown_token = line[idx + count]
						write_id_files(unknown_token, tags[idx + count], token_vocab, tag_vocab, unknown[1], f1, f2)
						count += 1



				# 加入句子间隔
				f1.write('\n')
				f2.write('\n')
	f3.close()


def get_trimmed_word2vec(trimmed_word2vec_path):
	'''
	从trimmed_word2vec文件中加载词向量，用于训练模型使用
	Args:
		trimmed_filename: 存储np matrix的路径
	Returns:
		matrix of embeddings (instance of np array)
	'''
	with np.load(trimmed_word2vec_path) as data:
		return data['embeddings']

def export_trimmed_word2vec(vocab, word2vec_path, trimmed_word2vec_path, dimension, add_unk = True):
	'''
	将出现过的字的embedding取出来，并且输出到trimmed_word2vec_path中，这样能够节省内存
	Args:
		vocab: dictionary vocab[word] = index
		word2vec_path: word2vec的路径
	'''
	embeddings = np.zeros([len(vocab), dimension])
	with open(word2vec_path) as f:
		for line in f:
			line = line.strip().split(' ')
			if len(line) <= dimension:
				continue
			# 取出字和向量
			char = line[0]
			embedding = [float(x) for x in line[1:]]

			if char in vocab:
				embeddings[vocab[char]] = np.asarray(embedding)
		# 加入UNK的embedding
		if add_unk:
			embeddings[vocab[UNK_TOKEN]] = np.asarray([0.0] * dimension)
				

	# 进行存储
	np.savez_compressed(trimmed_word2vec_path, embeddings = embeddings)


def get_vocabs_from_word2vec(filename):
	'''
	从word2vec中获取所有的字
	Args:
		filename: word2vec的路径
	'''
	token_vocab = []
	with open(filename) as f:
		for line in f:
			line = line.strip()
			items = line.split(' ')
			if len(items) < 50:
				continue
			token_vocab.append(items[0])
	return token_vocab


	pass
def get_vocabs_from_datasets(datasets):
	'''
	从datasets中获取词典
	Return:
		char_vocab, 
		event_type_vocab, 
		argument_role_vocab, 
		entity_type_vocab, 
		entity_sub_type_vocab
	'''
	char_vocab = set()
	event_type_vocab = set()
	argument_role_vocab = set()

	entity_type_vocab = set()
	entity_sub_type_vocab = set()

	for dataset in datasets:
		for event_type, argument_role, chars, entity_sub_types, entity_types, roles, distances_1, distances_2 in dataset:
			
			event_type_vocab.add(event_type)
			argument_role_vocab.add(argument_role)
			for char in chars:
				char_vocab.add(char)

			for entity_sub_type in entity_sub_types:
				entity_sub_type_vocab.add(entity_sub_type)
			for entity_type in entity_types:
				entity_type_vocab.add(entity_type)
			for role in roles:
				argument_role_vocab.add(role)
	# 对tag进行排序，保证每次都是一样的，哪怕是不同的训练集
	char_vocab = sorted(list(char_vocab))
	event_type_vocab = sorted(list(event_type_vocab))
	argument_role_vocab = sorted(list(argument_role_vocab))
	entity_type_vocab = sorted(list(entity_type_vocab))
	entity_sub_type_vocab = sorted(list(entity_sub_type_vocab))

	return char_vocab, event_type_vocab, argument_role_vocab, entity_type_vocab, entity_sub_type_vocab

def write_vocab(vocab, filename):
	'''
	'''
	with io.open(filename, 'w', encoding='utf-8') as f:
		for i, word in enumerate(vocab):
			if i != len(vocab) - 1:
				f.write(word + '\n')
			else:
				f.write(word)

def load_vocab(filename):
	'''
	从vocab文件中加载vocab
	Args:
		filename vocab文件路径
	Return:
		vocab: dict[word]=index
	'''
	vocab = {}
	idx = 0
	with open(filename) as f:
		for line in f:
			word = line.strip()
			vocab[word] = idx
			idx += 1
	return vocab

def id_to_vocab(vocab):
	"""
	返回一个dict: {id:'vocab'}
	vocab:
		load_vocab中得到的vocab dict[word]=index
	"""
	return {idx:word for word, idx in vocab.items()}


if __name__ == '__main__':
	# get_vocabs_from_word2vec('../data/char_word2vec.dat')
	vocab = {'a':1,'b':2}
	print(id_to_vocab(vocab))






	