#! /bin/bash
# $1 gpu
# $2 是否使用ground truth的entity

lambda_1_vars=(1 2 3 4 5 6 7 8 9 10)
lambda_2_vars=(1)

for lambda_1 in ${lambda_1_vars[@]}
do
	for lambda_2 in ${lambda_2_vars[@]}
	do
		echo $lambda_1
		echo $lambda_2
		python train_ltr.py --dirpath ../../arguments/english/ltr/results/$2/$lambda_1/$lambda_2 --add_nvidia $1 --use_ground_truth $2
	done
done