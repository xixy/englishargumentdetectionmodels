#! /bin/bash
# $1 gpu
# $2 是否使用ground truth的entity来进行预训练
# $3 是否在联合训练阶段训练detector

lambda_1_vars=(1 2 3 4 5)
#lambda_1_vars=(1)
lambda_2_vars=(1)

if [ $3 == 0 ]
then
   flag='do_not_train_detector'
else
   flag='train_detector'
fi
echo $flag

for lambda_1 in ${lambda_1_vars[@]}
do
	for lambda_2 in ${lambda_2_vars[@]}
	do
		echo $lambda_1
		echo $lambda_2
		python train_rl.py --dirpath ../../arguments/english/rl/results/$flag/$2/$lambda_1/$lambda_2 --add_nvidia $1 --use_ground_truth $2 --jointly_train_detector $3
	done
done