#! /bin/bash
# $1 gpu
# $2 是否添加argument role

lambda_1_vars=(1 2 3 4 5 6 7 8 9 10)
lambda_2_vars=(1)

if [ $2 == 0 ]
then
   flag='no_argument_roles'
else
   flag='argument_roles'
fi
echo $flag

for lambda_1 in ${lambda_1_vars[@]}
do
	for lambda_2 in ${lambda_2_vars[@]}
	do
		echo $lambda_1
		echo $lambda_2
		python train_single.py --dirpath ../../data/arguments/english/single/results/$flag/$lambda_1/$lambda_2 --add_nvidia $1 --add_argument_role $2
	done
done