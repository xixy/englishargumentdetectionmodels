# coding=utf-8
import sys
sys.path.append('./model/bert_as_feature/bert')
from configure import *
from model.vocab import *
from model.model_general_rl import *
from model.hyperparameter import *
from model.log_util import *
import argparse
from model.dataset import *
from model.entity_selector import *

def main(args):
	'''
	采用强化学习的方法来进行训练，总体分两步：
	1. 预训练Argument Detector
	2. 同时训练Argument Detector和Entity Selector
	'''
	# 1. 加载vocab和embeddings
	char_vocab = load_vocab(char_vocab_path)
	event_type_vocab = load_vocab(event_type_vocab_path)
	event_sub_type_vocab = load_vocab(event_sub_type_vocab_path)
	argument_role_vocab = load_vocab(argument_role_vocab_path)
	argument_role_bio_vocab = load_vocab(argument_role_vocab_bio_path)
	entity_type_vocab = load_vocab(entity_type_vocab_path)
	entity_sub_type_vocab = load_vocab(entity_sub_type_vocab_path)
	char_embeddings = get_trimmed_word2vec(trimmed_char_word2vec_path)
	pos_tag_vocab = load_vocab(pos_tag_vocab_path)

	# 2. 得到id化的训练数据
	train_dataset = dataset(train_data_id_path, idlized = True, shuffle = True)
	test_dataset = dataset(test_data_id_path, idlized = True, shuffle = False)
	dev_dataset = dataset(dev_data_id_path, idlized = True, shuffle = False)

	# 3. 构造模型
	hp = hyperparameter(args)
	hp_1 = hyperparameter(args)
	logger = get_logger(hp.log_path)
	vocabs = [char_vocab, event_type_vocab, event_sub_type_vocab, \
		argument_role_vocab, argument_role_bio_vocab, entity_type_vocab, entity_sub_type_vocab, pos_tag_vocab]
	
	# 3.1 创建entity selector以及hp
	selector = entity_selector(hp_1, logger,vocabs, char_embeddings)
	selector.build()

	# 3.2 构造argument detector
	model = cnn_model(hp, logger, vocabs, args, char_embeddings)
	model.build()

	# 3.3 构造session
	logger.info("Initializing tf session")
	sess = tf.Session()
	sess.run(tf.global_variables_initializer())
	saver = tf.train.Saver(tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES))
	model.sess = sess
	model.saver = saver

	# 4. 用从左往右的数据来预训练detector
	logger.info('开始预训练')
	model.train(train_dataset, dev_dataset)
	logger.info('预训练结束')

	# 5. 测试
	logger.info('开始测试')
	model.restore_session()
	output_all_file = open(model.hp.evaluate_output_all_file, 'w')
	output_wrong_file = open(model.hp.evaluate_output_wrong_file, 'w')
	output_class_wrong_file = open(model.hp.evaluate_output_class_wrong_file, 'w')

	model.evaluate(test_dataset, output_all_file, output_wrong_file, output_class_wrong_file)
	logger.info('测试结束')

	# 6. 联合训练entity selector和argument detector
	logger.info('开始联合训练')
	model.stage = 1
	model.train_rl(selector, train_dataset, dev_dataset, test_dataset)
	logger.info('联合训练结束')

	# 7. 进行测试
	logger.info('开始测试')
	model.restore_session()
	model.selector = selector
	model.evaluate(test_dataset, output_all_file, output_wrong_file, output_class_wrong_file)
	output_all_file.close()
	output_wrong_file.close()
	output_class_wrong_file.close()

if __name__ == '__main__':
	# python train_rl.py --dirpath '../results/result.1' --add_nvidia -1 --add_argument_role 1
	parser = argparse.ArgumentParser()
	parser.add_argument('--dirpath', required=True, type = str, help='输出路径')
	parser.add_argument('--add_nvidia',required=True, type = str, help='选择显卡编号')
	parser.add_argument('--add_argument_role', required=False, type = int, default = 1, help='是否添加argument role')
	parser.add_argument('--use_ground_truth', required = False, type = int, default = 0, help= '是否使用ground truth的argument来更新环境')
	parser.add_argument('--jointly_train_detector', required = False, type = int, default = 1, help= '是否在联合训练的时候训练detector模块')
	# 解析参数
	args = parser.parse_args()
	print(args.dirpath)
	# 设置显卡，如果是-1，就不使用显卡,使用CPU进行运算
	if args.add_nvidia in ['0', '1', '2', '3']:
		os.environ['CUDA_VISIBLE_DEVICES']=args.add_nvidia

	# args = parser.parse_args()
	main(args)






